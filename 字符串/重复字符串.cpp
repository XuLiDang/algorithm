class Solution {
public:
    // 如果一个长度为Len字符串s1能够由一个长度为subLen的子串s2重复多次构成的话，那么一定满足:
    // 1. Len是subLen的倍数; 2. s2一定是s1的前缀; 3. s1[j] = s1[j - subLen], j >= subLen
    // 那么任何不满足此条件的子串一定不能重复多次构成字符串。
    bool repeatedSubstringPattern(string s) {
        // 字串的长度
        int subLen = 0;
        // 主串的长度
        int Len = s.size();

        // 遍历选取子串
        // 若子串的长度大于主串的一半，则意味着该子串不可能重复多次构成主串
        for(int i = 0; i < Len / 2; i++) {
            bool match = true;
            // 当前选取的子串的长度
            subLen = i + 1;
            // Len不是subLen的倍数，则子串肯定不可能重复多次构成主串
            if(Len % subLen != 0)
                continue;          
            // 查看是否满足条件: s1[j] = s1[j - subLen], j >= subLen
            // 不满足则将match置为false并跳出循环
            for(int j = subLen; j < Len; j++) {
                if(s[j - subLen] != s[j]){
                    match = false;
                    break;
                }           
            }

            if(match)
                return true;
        }

        return false;
    }
};