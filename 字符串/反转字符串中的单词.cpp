class Solution {
public:
    void removeExtraSpace(string &s){
        int slow = 0, fast = 0;
        // 去掉字符串前面的空格
        while(s[fast] == ' ' && fast < s.size()) {
            fast++;
        }

        // 去掉字符串中间部分的冗余空格
        while(fast < s.size()) {
            if(s[fast] == ' ' && s[fast - 1] == s[fast]) {
                fast++;
            }else {
                s[slow] = s[fast];
                slow++;
                fast++;
            }
        }

        // slow 为新字符串最后一个元素的下标 + 1，因此其初始为 0。执行到这里时，新字符串中已不可能
        // 出现前置空格以及中间出现冗余空格的情况，但有可能会出现新字符串最后一个元素为空格的情况。
        // 因此，我们最后只需要去掉字符串末尾的空格即可。
        if(s[slow - 1] == ' ')
            s.resize(slow - 1);
        else
            s.resize(slow);
    }

    // 翻转函数
    void reverserStr(string &s, int start, int end) {
        int left = start, right = end;
        while(left < right) {
            swap(s[left],s[right]);
            left++;
            right--;
        }

    }

    string reverseWords(string s) {
        // 清楚字符串中多余的空格
        removeExtraSpace(s);
        // 翻转整个字符串
        reverserStr(s, 0, s.size() - 1);
        // start用于记录每个单词的初始下标
        int start = 0;

        // 翻转字符串里面的每个单词
        for(int i = 0; i < s.size(); i++) {
             if(s[i] == ' ') {
                // 翻转单词
                reverseStr(s, startIndex, i - 1);
                // 更新start为下一个单词的索引
                startIndex = i + 1;
            }
            // 翻转最后一个单词
            else if(i == s.size() - 1) 
                reverseStr(s, startIndex, i);
        }

        return s;
    }
};