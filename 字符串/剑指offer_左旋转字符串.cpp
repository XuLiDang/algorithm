class Solution {
public:
    void reverse(string& s, int start, int end) {
        while(start < end) {
            swap(s[start],s[end]);
            start++;
            end--;
        }
    }

    // 字符串左旋n位的思路：将前n个字符反转，再将剩余的字符反转，最后反转整个字符串
    // 字符串右旋n位的思路：从字符串末尾开始反转前n个字符，再将剩余字符反转，最后反转整个字符串
    string reverseLeftWords(string s, int n) {
        reverse(s, 0, n - 1);
        reverse(s, n, s.size() - 1);
        reverse(s, 0, s.size() - 1);
        return s;
    }
};