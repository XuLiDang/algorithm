class Solution {
public:
    string replaceSpace(string s) {
        int left, right, count = 0;
        int oldSize = s.size(), newSize;
        // 统计字符串中空格的个数
        for(int i = 0; i < s.size(); i++) {
            if(s[i] == ' ')
                count++;
        }

        // 扩充字符串s的大小，也就是每个空格替换成"%20"之后的大小
        s.resize(s.size() + count * 2);
        newSize = s.size();
        left = oldSize - 1;
        right = newSize - 1;
        // 从后先前将空格替换为"%20", left指向旧长度的末尾, right指向新长度的末尾
        // right <= left时就代表数组中已没有需要填充的空格了。
        while(left < right) {
            if(s[left] != ' ') 
                s[right] = s[left];
            else {
                s[right] = '0';
                s[right - 1] = '2';
                s[right - 2] = '%';
                right -= 2;
            }
            right--;
            left--;
        }
        return s;
    }
};