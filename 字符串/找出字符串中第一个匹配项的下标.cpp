class Solution {
public:
    // j指向前缀末尾位置，i指向后缀末尾位置。
    // j还表示下标i之前(包括下标i)的字符串的最长相同前后缀长度
    void getNext(int* next, const string& s) {
        // 初始化操作，这里的next数组就等于前缀表
        // next[i]代表的就是下标i之前(包括下标i)的字符串的最长相同前后缀长度
        // j = 0，即从第二个字符开始匹配
        int j = 0;
        // 第一个字符构成的字符串的最长相同前后缀长度为0
        next[0] = 0;

        for(int i = 1; i < s.size(); i++) {
            // 前后缀匹配不成功，前缀需要回退到next[j - 1]的位置
            while (j > 0 && s[i] != s[j]) {
                j = next[j - 1];
            }
            // 前后缀匹配成功
            // 注意j还表示下标i之前(包括下标i)的字符串的最长相同前后缀长度
            if (s[i] == s[j]) {
                j++;
            }
            // 更新下标i之前(包括下标i)的字符串的最长相同前后缀长度
            next[i] = j;
        }
    }
    int strStr(string haystack, string needle) {
        if (needle.size() == 0) {
            return 0;
        }
        int next[needle.size()];
        getNext(next, needle);
        int j = 0;
        int i = 0;
        // 执行匹配操作
        while(i < haystack.size()) {
             // 主串和模式串失配，失配的位置为其他元素的下标
             // 此时需要通过next数组来更新模式串的下标，并根据该下标继续匹配
             // 因此这条语句必须要放在前面 
            while(haystack[i] != needle[j] && j > 0)
                j = next[j - 1];

            // 主串和模式串失配，并且失配的位置为模式串的第一个元素下标
            if(haystack[i] != needle[j] && j == 0)
                i++;

            // 主串与模式串匹配
            if(haystack[i] == needle[j]) {
                i++;
                j++;
            }

            // 模式串遍历完毕，返回其第一个匹配项在主串中的下标
            if(j == needle.size())
                return (i - needle.size());          
        }
       
        return -1;
    }
};

// 子串："ABCD"
// 前缀："A, AB, ABC"
// 后缀："D, CD, BCD"
// 最长前缀后缀长度：0