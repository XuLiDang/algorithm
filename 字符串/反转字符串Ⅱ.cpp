class Solution {
public:
    // 参数要加引用
    void reverse_Substr(string& s, int begin, int end) {
        while(begin < end) {
            swap(s[begin], s[end]);
            begin++;
            end--;
        }
    }

    string reverseStr(string s, int k) {
        // 先判断当前剩余元素个数是否 大于等于 k，并且进行对应的处理，随后再往后移动 2 * k 步。
        for(int i = 0; i < s.size(); i += 2 * k) {
            // 当前剩余元素个数小于 k，因此将剩余字符全部翻转
            if(i + k > s.size())
                reverse_Substr(s, i, s.size() - 1);
            // 当前剩余元素个数大于等于 k，因此将前 k 个字符进行翻转，其他字符保持原样
            else
                reverse_Substr(s, i, i + k - 1);  
        }
        return s;
    }


};