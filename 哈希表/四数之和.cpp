class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
        int left, right;
        // 结果数组
        vector<vector<int>> result;
        // 对数组进行排序
        sort(nums.begin(),nums.end());

        for(int i = 0; i < nums.size(); i++) {
            // 剪枝处理，nums[i] >= 0 用来防止target为负数
            if (nums[i] > target && nums[i] >= 0) 
                break; // 这里使用break，统一通过最后的return返回
            
            // 对a进行去重，如果当前遍历的元素值等于上一次遍历的元素
            // 那么就代表基于当前遍历元素而找到的四元组一定会与之前的重复
            if(i > 0 && nums[i] == nums[i - 1]) continue;

            for(int k = i + 1; k < nums.size(); k++) {
                // 剪枝处理，nums[k] + nums[i] >= 0 用来防止target为负数
                if(nums[k] + nums[i] > target && nums[k] + nums[i] >= 0)
                    break;

                // 对b进行去重，如果当前遍历的元素值等于上一次遍历的元素
                // 那么就代表基于当前遍历元素而找到的四元组一定会与之前的重复
                if(k > i + 1 && nums[k] == nums[k - 1]) continue;
                left = k + 1;
                right = nums.size() - 1;
                while(left < right) {
                    // nums[k] + nums[i] + nums[left] + nums[right] > target 会溢出
                    if((long)nums[i] + nums[k] + nums[left] + nums[right] == target) {
                        result.push_back(vector<int>{nums[i], nums[k], nums[left], nums[right]});
                        // 去重逻辑应该放在找到一个四元组之后，对c和d去重
                        // 当前右指针指向的元素等于下一个元素的值，那么右指针指向
                        // 下一元素时，所找到的四元组必定会与当前找到的四元组重复
                        while(left < right && nums[right] == nums[right - 1]) right--;
                        // 去重逻辑应该放在找到一个四元组之后，对c和d去重
                        // 当前左指针指向的元素等于下一个元素的值，那么左指针指向
                        // 下一元素时，所找到的四元组必定会与当前找到的四元组重复
                        while(left < right && nums[left] == nums[left + 1]) left++;

                         // 找到答案时，双指针同时收缩
                        right--;
                        left++;
                    }
                    else if((long)nums[i] + nums[k] + nums[left] + nums[right] < target)
                        left++;
                    else
                        right--;
                }
            }
        }

        return result;
    }
};