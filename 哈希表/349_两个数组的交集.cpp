class Solution {
public:
    // unordered_set的逻辑: 向unordered_set插入一个key，它会根据哈希函数找到对应的桶的索引
    // 随后将该桶的值设置为key。因此在unordered_set查找key的对应桶的值时，返回的也是key
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        // 存放结果，之所以用set是为了给结果集去重
        unordered_set<int> result_set;
        // 将nums1的所有值都插入到nums_set中，nums_set中没有重复的元素
        unordered_set<int> nums_set(nums1.begin(), nums1.end()); 
        // 遍历nums2
        for(int num : nums2) {
            // nums2中的元素同样也存在于nums_set中
            if(nums_set.find(num) != nums_set.end()) {
                // 将交集存放在result_set中，其里面同样没有重复的元素
                result_set.insert(num);
            }
        }
        // 返回结果数组
        return vector<int>(result_set.begin(), result_set.end());
    }
};