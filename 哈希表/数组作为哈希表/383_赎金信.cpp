class Solution {
public:
    // 当涉及到需要统计元素出现的次数时，可考虑用数组或map充当哈希表来处理
    bool canConstruct(string ransomNote, string magazine) {
        // 用数组充当简单的哈希表，用于存放每个字符的出现次数
        int record[26] = {0};

        // 记录magazine中每个字符的出现次数
        for(int i = 0; i < magazine.size(); i++)
           record[magazine[i] - 'a']++;

        for(int i = 0; i < ransomNote.size(); i++) {
            // 如果小于零说明ransomNote里出现的字符，magazine没有
            // 或者字符的出现次数大于magazine中出现的次数
            if(record[ransomNote[i] - 'a'] == 0)
                return false;
            else
                // 遍历ransomNote，在record里对应的字符个数做--操作
                record[ransomNote[i] - 'a']--;
        }
        
        return true;
    }
};