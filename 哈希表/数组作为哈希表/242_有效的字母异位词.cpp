class Solution {
public:
    // 采用哈希表的方法来完成
    bool isAnagram(string s, string t) {
        // 采用数组作为哈希表
        int record[26] = {0};
        // 在哈希表中记录字符串中每个字符出现的次数，可将字符看作为key
        // 而哈希函数则是key - 'a'，从而求出每个字符在哈希表中的下标
        // 最后将索引对应的元素值+1，这样就能记录每个字符出现的次数
        for(int i = 0; i < s.size(); i++) 
            record[s[i] - 'a']++;
        
        // 采取跟上面一样的方法，不同的点在于将索引对应的元素值-1 
        for(int i = 0; i < t.size(); i++)
            record[t[i] - 'a']--;

        // 最后检查哈希表，所有元素值都为0便代表两个字符串为字母异位词
        for(int i = 0; i < 26; i++) {
            // 任一元素值不为0都返回false
            if(record[i] != 0){
                return false;
            }
        }
        // record数组所有元素都为零0，说明字符串s和t是字母异位词
        return true;
    }
};