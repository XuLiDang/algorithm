class Solution {
public:
    // 该题的简化版本：给你两个整型数组A和B，两者长度都是N，请计算有多少个元组(i,j)满足
    // 0 <= i, j < n 以及 A[i] + A[j] == 0; 此时我们也是定义一个map，key是数组A中元素的值
    // value则是同一元素值出现的次数。随后遍历数组B，在每次遍历过程中，都要查看map中是否存在值为
    // "0 - b" 的key，如果存在则将值为"0 - b" 的key的出现次数添加到统计值count当中。
    int fourSumCount(vector<int>& nums1, vector<int>& nums2, vector<int>& nums3, vector<int>& nums4) {
        // 定义一个map，key是a和b两数之和，value放两数之和出现的次数
        // a和b分别是数组1和数组2中的一个元素。
        unordered_map<int,int> umap;
        // 统计a + b + c + d = 0 出现的次数
        int count = 0;

        // 遍历nums1和nums2数组，统计两个数组元素之和以及和出现的次数，放到map中
        for(int a : nums1) {
            for(int b : nums2) {
                umap[a + b] ++;
            }
        }

        // 以同样的方式遍历nums3和nums4数组，若map中出现值为0 - (c + d)的key
        // 则代表此时c + d + key = 0
        for(int c : nums3) {
            for(int d : nums4) {
                if(umap.find(0 - (c + d)) != umap.end()) {
                    count += umap[0 - (c + d)];
                }
            }
        }

        // 返回统计值count
        return count;
    }
};