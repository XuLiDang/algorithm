class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        int left = 0, right = 0;
        // 结果数组
        vector<vector<int>> result;
        // 对数组进行排序，排完序之后，数值相同的元素就会挨在一起，后面的逻辑要基于这个基础才有效
        sort(nums.begin(), nums.end());
        // 遍历选取 a 
        for(int i = 0; i < nums.size(); i++) {
            // 排序之后如果第一个元素已经大于零，那么无论如何组合都不可能凑成三元组，直接返回结果就可以了
            if (nums[i] > 0) {
                return result;
            }

            // 对 a 进行去重，如果当前遍历的元素值等于上一次遍历的元素
            // 那么就代表基于当前遍历元素而找到的三元组一定会与之前的重复
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }

            left = i + 1;
            right = nums.size() - 1;
            while(left < right) { 
                // 三数之和正好等于 0
                if(nums[i] + nums[left] + nums[right] == 0){
                    result.push_back(vector<int>{nums[i], nums[left], nums[right]});

                    // 去重逻辑应该放在找到一个三元组之后，对b 和 c去重
                    // 当前右指针指向的元素等于下一个元素的值，那么右指针指向
                    // 下一元素时，所找到的三元组必定会与当前找到的三元组重复
                    while (right > left && nums[right] == nums[right - 1]) right--;
                    // 当前左指针指向的元素等于下一个元素的值，那么左指针指向
                    // 下一元素时，所找到的三元组必定会与当前找到的三元组重复
                    while (right > left && nums[left] == nums[left + 1]) left++;

                    // 找到答案时，双指针同时收缩
                    right--;
                    left++;
                }
                // 三数之和小了，left 就向右移动
                else if (nums[i] + nums[left] + nums[right] < 0)
                    left++;
                // 三数之和大了，right 就向左移动
                else
                    right--;
            }
        }

        return result;
    }
};