class Solution {
public:
    // 求出正整数每个位置上的数字的平方和
    int getSum(int n) {
        int sum = 0;
        // 从个位数开始，求出每个位置上的平方数
        // 并用sum记录所有平方数的和
        while(n) {
            sum += (n % 10) * (n % 10);
            n = n / 10;
        }
        return sum;
    }

    // 返回结果的条件分别是平方数的和为1或者求和过程中出现无限循环
    // 判断一个元素是否出现时或是否重复出现过，便可以考虑用set来处理
    bool isHappy(int n) {
        int sum = 0;
        unordered_set sums_set;

        while(1) {
            sum = getSum(n);
            // 平方数的和为1直接返回true
            if(sum == 1)
                return true;
            else {
                // 查看当前平方数的和是否出现在set中，出现了就代表出现了重复的平方和
                // 而重复的平方和必然会导致后面求和过程会陷入无限循环，因此平方和不可能为1
                if(sums_set.find(sum) != nums_set.end())
                    return false;
                else {
                    // 将结果放入set中，
                    sums_set.insert()
                    // 更新数字
                    n = sum;
                }
            }

        }
    }
};