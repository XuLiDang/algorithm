class Solution {
public:
    // 使用一个集合来存放已经遍历过的元素以及对应的下标。在遍历数组时
    // 首先查看集合中是否存在特定元素，该元素值与当前数组中的元素值
    // 相加刚好等于target。若有则返回数组当前元素下标与集合中元素的
    // 下标，若没有则将当前数组元素加入到集合中并继续往下遍历数组。
    // 我们使用map充当这个集合的话，元素值 = key，下标 = value
    vector<int> twoSum(vector<int>& nums, int target) {
        unordered_map <int,int> nums_map;
        for(int i = 0; i < nums.size(); i++) {
            // 根据 key 在 map 中查找对应的键值对
            auto tier = nums_map.find(target - nums[i]);{}
            // 键值对存在则直接返回两个元素的下标
            if(tier != nums_map.end())
                return {i,tier->second};
            else
                // 不存在则需要将当前元素的值作为 key，而将索引作为 value，并且已键值对的形式插入 map 中
                nums_map.insert(pair<int, int>(nums[i],i));        
        }

        return {};
    }
};