class MyQueue {
public:
    // 输入栈
    stack<int> stackIn;
    // 输出栈
    stack<int> stackOut;

    MyQueue() {

    }
    // 入队时仅需要向输入栈压人元素即可
    void push(int x) {
        stackIn.push(x);
    }
    
    // 出队操作，当输出栈未空时只需将输出栈的元素逐个出栈即可
    // 但当输出栈已空时则需要先将输入栈的全部元素出栈，并入栈
    // 到输出栈中，随后再将输出栈的元素逐个出栈
    int pop() {
        // 判断输出栈是否为空
        if(stackOut.empty()){   
            // 将输入栈的全部元素出栈，并入栈到输出栈中
            while(!stackIn.empty()) {
                stackOut.push(stackIn.top());
                stackIn.pop();
            }
        }

        // 获取栈顶元素
        int result = stackOut.top();
        // 将输出栈的元素出栈
        stackOut.pop();

        return result;
    }
    
    // 返回队列开头的元素
    int peek() {
         // 判断输出栈是否为空
        if(stackOut.empty()){
            // 将输入栈的全部元素出栈，并入栈到输出栈中   
            while(!stackIn.empty()) {
                stackOut.push(stackIn.top());
                stackIn.pop();
            }
        }

        // 获取栈顶元素
        int result = stackOut.top();
        return result;
    }
    
    // 判断队列是否为空
    bool empty() {
        if(stackIn.empty() && stackOut.empty())
            return true;
        else
            return false;
    }
};

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue* obj = new MyQueue();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->peek();
 * bool param_4 = obj->empty();
 */