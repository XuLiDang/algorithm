class Solution {
public:
    // 寻找区间[left,right]内的最大值
    int maxEle(int left, int right, vector<int>& nums) {
        int max = nums[left];
        for(int i = left; i <= right; i++) {
            if(nums[i] > max)
                max = nums[i];
        }

        return max;
      
    }

    vector<int> maxSlidingWindow1(vector<int>& nums, int k) {
        int left, right;
        // 结果数组
        vector<int> result;

        for(left = 0, right = k - 1; right < nums.size(); left++, right++)
            // 将区间区间[left,right]内的最大值加入结果数组中
            result.push_back(maxEle(left,right,nums));

        return result;
    }

    vector<int> maxSlidingWindow2(vector<int>& nums, int k) {
        // 通过双端队列维持一个单调递减队列，队头元素的值最大
        // 我们需要通过这个队列来维护当前滑动窗口的最大值
        deque<int> window;
        // 结果数组
        vector<int> result;
        // 遍历整数数组
        for(int i = 0; i < nums.size(); i++) {
            // 单调队列的特点是: 将元素加入单调队列时，会将所有小于它的元素移除队列，直到遇到值比它大的元素
            // 因此我们需要从队尾开始，将所有值小于nums[i]的元素出队
            while(!window.empty() && nums[i] > window.back())
                window.pop_back();
            // 将nums[i]入队
            window.push_back(nums[i]);

            // 已遍历的元素个数等于或大于滑动窗口的长度，因此可以判断此时窗口的最大值了
            if(i >= k - 1) {
                // 将单调队列中的队头元素放到结果数组中
                result.push_back(window.front());
                // 单调队列中维护的是当前滑动窗口的最大值，而滑动窗口移动一次移除的只有最左边的元素
                // 因此，若此时滑动窗口最左边的元素刚好是最大值，那么窗口移动过后，最大值肯定不属于
                // 当前的滑动窗口，因此需要将该元素从队列中移除。
                if(!window.empty() && window.front() == nums[i - k + 1])
                    window.pop_front();
            }
        }

        return result;
    }

};