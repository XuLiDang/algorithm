class Solution {
public:
    // 使用仿函数重载 < 操作符
    class myComparison{
    public:
        // second值越小，优先级就越高，因此符合小根堆的要求。例如a.second = 5
        // b.second = 10，那么 a < b 返回的就为false
        bool operator()(pair<int,int>& p1, pair<int,int>& p2) {
            return p1.second > p2.second;
        }
    };

    vector<int> topKFrequent(vector<int>& nums, int k) {
        // 要统计元素出现频率
        unordered_map<int, int> map;
        for(int i = 0; i < nums.size(); i++) {
            map[nums[i]] ++;
        }

        // 按照频率排序
        // 定义一个小根堆，大小为k
        priority_queue<pair<int,int>, vector<pair<int,int>>, myComparison> pri_queue;

        // 每次都先将元素直接插入堆中，当堆中的元素个数大于K时，便将堆顶元素出队
        // 按照此方法，堆中最多只能有k个元素，并且是出现频率最高的k个元素
        for(auto& a:map) {
            pri_queue.push(a);
            // 优先队列的元素个数大于K则将堆顶元素出队，堆顶元素始终都为堆中最小的元素
            if(pri_queue.size() > k)
                pri_queue.pop();
        }

        // 结果数组
        vector<int> result(k);
        // 逆序弹出优先队列的元素，并将其放入数组中
        for(int i = k - 1; i >= 0 && !pri_queue.empty(); i--) {
            result[i] = pri_queue.top().first;
            pri_queue.pop();
        }
        
        return result;
    }
};