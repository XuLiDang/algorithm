class Solution {
public:
    string removeDuplicates(string s) {
        stack<char> st;
        // 左右指针，用于反转字符串
        int left,right;
        // 最后返回的结果字符串
        string result = "";

        // 遍历字符串，将与栈顶元素不同的字符压入栈中
        // 遇到与栈顶元素相同的字符则弹出栈顶元素
        for(int i = 0; i < s.size(); i++) {
            if(st.empty() || s[i] != st.top()) 
                st.push(s[i]);
            else
                st.pop();
        }

        // 将栈的元素转换为字符串
        while(!st.empty()) {
            result += st.top();
            st.pop();
        }

        left = 0;
        right = result.size() - 1;
        // 将得到的字符串翻转，得到最终的结果字符串
        while(left < right) {
            swap(result[left],result[right]);
            left++;
            right--;
        }

        return result;
    }
};