class Solution {
public:
    bool isValid(string s) {
        stack<char> st;
        // 循环遍历字符串
        for(int i = 0; i < s.size(); i++) {
            // 遇到左括号则直接入栈
            if(s[i] == '(' || s[i] == '{' || s[i] == '[')
                st.push(s[i]);
            // 遇到右括号的处理要稍微麻烦些
            else {
                // 首先判断栈是否为空，为空则代表右括号过多，括号匹配失败
                if(st.empty()) 
                    return false;

                if(s[i] == ')' && st.top() == '(') {
                    st.pop();
                    continue;
                }
                else if(s[i] == '}' && st.top() == '{') {
                    st.pop(); 
                    continue;
                }
                else if(s[i] == ']' && st.top() == '['){
                    st.pop();
                    continue;
                }
                else
                    return false;             
            }
        }
        // 字符串遍历完毕后查看栈是否为空，不为空则代表左括号过多，括号匹配失败
        if(!st.empty()) 
            return false;
        // 满足所有条件，返回true
        return true;
    }
};