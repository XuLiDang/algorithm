/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    // 查找节点p，q是否存在以root为根节点的二叉树中。存在则返回相应的节点，相应的节点可以是
    // 这棵二叉树中的任何节点，否则返回null
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        if(root == NULL) return NULL;
        // 找到了p或q节点则直接返回，无需继续往下查找
        if(root == p || root == q) return root;

        // 查找节点p，q是否存在以root->left为根节点的二叉树中。存在则返回相应的节点，否则返回null
        TreeNode* left = lowestCommonAncestor(root->left, p, q);
        // 查找节点p，q是否存在以root->right为根节点的二叉树中。存在则返回相应的节点，否则返回null
        TreeNode* right = lowestCommonAncestor(root->right, p, q);

        // 如果在当前节点的左右子树中分别找到了给定的两个节点，则当前节点就是最近的公共祖先
        if(left && right)
            return root;

        // 只有左子树找到了给定的节点，则说明要么两个节点都位于当前节点的左子树，要么只有一个节点位于
        // 左子树，而另一个节点并不存在于以当前root节点作为根节点的二叉树中。对于第一种情况，左子树返
        // 回的肯定是两个节点最近的公共祖先。而对于第二种情况，左子树返回的则是值为p或q的节点。但无论是
        // 哪一种情况，都说明当前root节点并不是最近的公共祖先，因此直接把左子树返回的节点交给上层处理。
        else if(left && !right)
            return left;
        
        // 只有右子树找到了给定的节点，则说明要么两个节点都位于当前节点的右子树，要么只有一个节点位于
        // 右子树，而另一个节点并不存在于以当前root节点作为根节点的二叉树中。对于第一种情况，右子树返
        // 回的肯定是两个节点最近的公共祖先。而对于第二种情况，右子树返回的则是值为p或q的节点。但无论是
        // 哪一种情况，都说明当前root节点并不是最近的公共祖先，因此直接把右子树返回的节点交给上层处理。
        else if(!left && right)
            return right;

        // 以root为根节点的二叉树中不存在给定的两个节点，则返回NULL
        else
            return NULL;
    }
};