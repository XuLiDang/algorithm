/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        while(root) {
            // p和q节点的值均小于root节点，则两个节点的最近公共祖先只存在于root节点的左子树中
            if(p->val < root->val && q->val < root->val) 
                root = root->left;
            // p和q节点的值均大于root节点，则两个节点的最近公共祖先只存在于root节点的右子树中
            else if(p->val > root->val && q->val > root->val) 
                root = root->right;
            // 一个节点的值大于root节点，一个节点的值小于root节点，此时表明root节点就是两个节点的最近公共祖先
            else
                return root;

        }

        return NULL;
    }
};