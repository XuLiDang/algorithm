/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    void invert(TreeNode* root) {
        if(root == NULL) return;
        // 翻转当前节点左右孩子
        TreeNode* tmp = root->left;
        root->left = root->right;
        root->right = tmp;
        // 翻转当前节点的左孩子
        invert(root->left);
        // 翻转当前节点的右孩子
        invert(root->right);
    }   

    TreeNode* invertTree(TreeNode* root) {
        invert(root);
        return root;
    }
};