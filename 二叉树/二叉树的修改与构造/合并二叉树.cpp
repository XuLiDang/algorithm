/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {
        // 两者均为空则返回NULL
        if(root1 == NULL && root2 == NULL) return NULL;
        // root2为空，合并后就是root1
        if(root1 != NULL && root2 == NULL) return root1;
        // root1为空，合并后就是root2
        if(root1 == NULL && root2 != NULL) return root2;

        // 修改root1的数值，将其作为合并后的新节点
        root1->val += root2->val;

        // 合并两棵树的左孩子
        root1->left = mergeTrees(root1->left, root2->left);
        // 合并两棵树的右孩子
        root1->right = mergeTrees(root1->right, root2->right);

        return root1;
    }

};