/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 区间：左闭右开
    TreeNode* traversal(vector<int>& preorder, int preorderBegin, int preorderEnd, vector<int>& inorder, int inorderBegin, int inorderEnd) {
        // 当前前序遍历序列中的元素个数为0则直接返回NULL
        if(preorderBegin == preorderEnd) return NULL;

        // 前序遍历序列第一个元素，就是当前遍历序列的中间节点
        int rootValue = preorder[preorderBegin];
        // 根据节点值创建当前遍历序列的中间节点
        TreeNode* root = new TreeNode(rootValue);

         // 前序遍历序列中只有一个元素
        if(preorderEnd - preorderBegin == 1) return root; 

        int inorder_index = 0;
        // 查找前遍历序列的第一个元素在中序遍历序列中的下标
        for(int i = inorderBegin; i < inorderEnd; i++) {
            if(inorder[i] == rootValue)
                inorder_index = i;
        }

         // 切割中序数组
        // 左中序区间，左闭右开[leftInorderBegin, leftInorderEnd)
        int leftInorderBegin = inorderBegin;
        int leftInorderEnd = inorder_index;
        // 右中序区间，左闭右开[rightInorderBegin, rightInorderEnd)
        int rightInorderBegin = inorder_index + 1;
        int rightInorderEnd = inorderEnd;

        // 切割前序数组
        // 左前序区间，左闭右开[leftPostorderBegin, leftPostorderEnd)、
        // 注意，第一个元素已作为节点了，因此需要将其排除掉
        int leftPreorderBegin = preorderBegin + 1;
        // inorder_index - inorderBegin 代表的是左中序区间的大小
        int leftPreorderEnd = preorderBegin + 1 + inorder_index - inorderBegin;
        // 右前序区间，左闭右开[rightPostorderBegin, rightPostorderEnd)
        int rightPreorderBegin = preorderBegin + 1 + inorder_index - inorderBegin; 
        int rightPreorderEnd = preorderEnd;

        // 根据左中序与左前序区间，寻找左区间的根节点，并作为当前节点的左孩子
        root->left = traversal(preorder, leftPreorderBegin, leftPreorderEnd, inorder, leftInorderBegin, leftInorderEnd);
        // 根据右中序与右前序区间，寻找右区间的根节点，并作为当前节点的右孩子
        root->right = traversal(preorder, rightPreorderBegin, rightPreorderEnd, inorder, rightInorderBegin, rightInorderEnd);

        return root;
    }

    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        if (inorder.size() == 0 || preorder.size() == 0) return NULL;

        // 参数坚持左闭右开的原则
        return traversal(preorder, 0, preorder.size(), inorder, 0, inorder.size());
    }
};