/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 根据当前的整数序列构建二叉树，并且根节点为序列中值最大的元素。左闭右开原则
    TreeNode* traversal(vector<int>& nums, int begin, int end) {
        // 数组序列为空则直接返回NULL
        if(end == begin) return NULL;

        // 从当前数组序列中找到最大值，并记录其索引
        int max = -1; int index = 0;
        for(int i = begin; i < end; i++) {
            if(nums[i] > max){
                max = nums[i];
                index = i;
            }
        }
        // 根据最大值构建树节点
        int rootValue = max;
        TreeNode* root = new TreeNode(rootValue);

        // 数组序列只有一个元素则直接返回刚创建的树节点
        if(end - begin == 1) return root;

        // 切割数组
        int leftBegin = begin;
        int leftEnd = index;
        int rightBegin = index + 1;
        int rightEnd = end;

        // 将左区间的最大值作为当前节点的左孩子
        root->left = traversal(nums, leftBegin, leftEnd);
        // 将右区间的最大值作为当前节点的右孩子
        root->right = traversal(nums, rightBegin, rightEnd);
        return root;
    }


    TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
        return traversal(nums, 0, nums.size());
    }
        
};