/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 区间：左闭右开
    TreeNode* traversal(vector<int>& inorder, int inorderBegin, int inorderEnd, vector<int>& postorder, int postorderBegin, int postorderEnd) {
        // 当前后序遍历序列中的元素个数为0则直接返回NULL
        if(postorderBegin == postorderEnd) return NULL;

        // 后序遍历序列最后一个元素，就是当前遍历序列的中间节点
        int rootValue = postorder[postorderEnd - 1];
        // 根据节点值创建当前遍历序列的中间节点
        TreeNode* root = new TreeNode(rootValue);

        // 后序遍历序列中只有一个元素
        if(postorderEnd - postorderBegin == 1)
            return root;

        int inorder_index = 0;
        // 查找后序遍历序列的最后一个元素在中序遍历序列中的下标
        for(int i = inorderBegin; i < inorderEnd; i++) {
            if(inorder[i] == rootValue) {
                inorder_index = i;
                break;
            }
        }

        // 切割中序数组
        // 左中序区间，左闭右开[leftInorderBegin, leftInorderEnd)
        int leftInorderBegin = inorderBegin;
        int leftInorderEnd = inorder_index;
        // 右中序区间，左闭右开[rightInorderBegin, rightInorderEnd)
        int rightInorderBegin = inorder_index + 1;
        int rightInorderEnd = inorderEnd;

        // 切割后序数组
        // 左后序区间，左闭右开[leftPostorderBegin, leftPostorderEnd)
        int leftPostorderBegin = postorderBegin;
        // inorder_index - inorderBegin 代表的是左中序区间的大小
        int leftPostorderEnd = postorderBegin + inorder_index - inorderBegin;
        // 右后序区间，左闭右开[rightPostorderBegin, rightPostorderEnd)
        int rightPostorderBegin = postorderBegin + inorder_index - inorderBegin;
        // 注意，最后一个元素已作为节点了，因此需要将其排除掉
        int rightPostorderEnd = postorderEnd - 1;

        // 根据左中序与左后序区间，寻找左区间的根节点，并作为当前节点的左孩子
        root->left = traversal(inorder, leftInorderBegin, leftInorderEnd, postorder, leftPostorderBegin, leftPostorderEnd);
        // 根据右中序与右后序区间，寻找右区间的根节点，并作为当前节点的右孩子
        root->right = traversal(inorder, rightInorderBegin, rightInorderEnd, postorder, rightPostorderBegin, rightPostorderEnd);

        return root;
    }


    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
       if(inorder.size() == 0 || postorder.size() == 0) return NULL;
       return traversal(inorder, 0, inorder.size(), postorder, 0, postorder.size());
    }
};