/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 记录当前节点值的频率
    int times = 0;
    // 指向前一个节点的指针
    TreeNode* pre = NULL;

    void traversal(TreeNode* root, vector<int> &result, int &max_times) {
        if(root == NULL) return;
        // 遍历左子树
        traversal(root->left, result, max_times);

        // 第一个元素
        if(pre == NULL){
            times = 1;
        }
        // 当前节点值与上一个节点值相同
        else if(pre->val == root->val) {    
            // 增加当前节点值的出现频率    
            times++;                   
        }
        // 当前节点值不同于上一个节点值
        else {
            // 将当前节点值的出现频率设置为1
            times = 1;
        }
        // 将当前节点作为前一个节点
        pre = root;

        // 当前节点值频率等于最高频率
        if(times == max_times) {
            // 将当前节点值放入结果数组中
            result.push_back(root->val);
        }
        // 当前节点值频率大于最高频率
        else if(times > max_times) {
            // 更新最高频率
            max_times = times;     
            // 清除数组中的所有节点值
            result.clear();
            // 将当前节点值放入结果数组中
            result.push_back(root->val);       
        }
        // 遍历右子树
        traversal(root->right, result, max_times);
    }

    vector<int> findMode(TreeNode* root) {
        // 结果数组
        vector<int> result;
        // 记录最高频率
        int max_times = 0;
        traversal(root, result, max_times);
        // 返回结果数组
        return result;
    }
};