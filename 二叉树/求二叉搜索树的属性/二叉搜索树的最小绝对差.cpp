/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 记录最小绝对差值
    int result = INT_MAX;
    // 记录中序遍历中当前节点的前一个节点
    TreeNode* pre = NULL;
    void traversal(TreeNode* root) {
        // 当前节点为空则直接返回NULL
        if(root == NULL) return;

        // 遍历左子树
        traversal(root->left);

        // 前一个节点不为空则计算当前节点与前一个节点的差值
        // 并且将差值与当前最小绝对差值进行对比
        if(pre != NULL) {
            result = min(result, root->val - pre->val);
        }
        // 将当前节点作为前一个节点
        pre = root;

        // 遍历右子树
        traversal(root->right);
    }

    int getMinimumDifference(TreeNode* root) {
        // 利用二叉搜索树中序遍历一定升序的特性，当前一个节点不空时，便计算当前节点与前一个节点的差值
        // 并且将差值与当前最小绝对差值进行对比，前者小于后者则更新差值
        traversal(root);
        return result;
    }
};