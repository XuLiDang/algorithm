/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 用来记录中序遍历中当前节点的前一个节点
    TreeNode* pre = NULL; 
    bool isValidBST(TreeNode* root) {
        // 当前节点为空直接返回true
        if (root == NULL) return true;
        
        // 判断左子树是否为二叉搜索树
        bool left = isValidBST(root->left);

        // 将当前节点的值与前一个节点的值(不为空)进行对比
        if (pre != NULL && pre->val >= root->val) 
            return false;
        // 将当前节点作为前一个节点
        pre = root; 

        // 判断右子树是否为二叉搜索树
        bool right = isValidBST(root->right);

        return left && right;
    }
};