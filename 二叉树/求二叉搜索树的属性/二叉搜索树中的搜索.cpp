/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* traversal(TreeNode* root, int& val) {
        if(root == NULL) return NULL;
        if(root->val == val) return root;

        if(val < root->val)
            return searchBST(root->left, val);
        else
            return searchBST(root->right, val);
    }

    // 递归法
    TreeNode* searchBST(TreeNode* root, int val) {
      return traversal(root, val);
    }

    // 迭代法
    TreeNode* searchBST(TreeNode* root, int val) {
      if(root == NULL) return NULL;

      while(root != NULL) {
        if(val < root->val) 
            root = root->left;
        else if (val > root->val) 
            root = root->right;
        else 
            return root;
      }

      return NULL;
    }
};