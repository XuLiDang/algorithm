/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* insertIntoBST(TreeNode* root, int val) {
        // 二叉搜索树为空则将新值作为根节点
        if(root == NULL) {
            root = new TreeNode(val);
            return root; 
        }

        TreeNode *cur = root;
        TreeNode *pre = NULL;
        // 查找新值的插入位置，循环结束后，pre指向的是新值的父节点
        while(cur) {
            if(val < cur->val) {
                pre = cur;
                cur = cur->left;
            }
            else if(val > cur->val) {
                pre = cur;
                cur = cur->right; 
            } 
        }

        // 根据新值创建节点
        TreeNode* newNode = new TreeNode(val);
        // 新节点的值小于其父节点的值
        if(newNode->val < pre->val)
            pre->left = newNode;
        // 新节点的值大于其父节点的值
        else
            pre->right = newNode;

        // 返回新二叉搜索树的根节点
        return root;
    }
};