/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public: 
    // 修改以root为根节点的二叉搜索树，并返回更新后的树的根节点
    TreeNode* trimBST(TreeNode* root, int low, int high) {     
        if (!root) return NULL;

        // root节点的值小于low，则其左子树肯定也不在范围内，此时只需要对右子树继续进行修剪
        // 随后只需要向上层返回修剪过后的右子树的根节点。算法思想是依靠二叉搜索树的特性进行修剪
        if (root->val < low) return trimBST(root->right, low, high);
        // root节点的值大于high，则其右子树肯定也不在范围内，此时只需要对左子树继续进行修剪
        // 随后只需要向上层返回修剪过后的左子树的根节点
        if (root->val > high) return trimBST(root->left, low, high);

        // 修剪左子树，并将更新后的树的根节点作为root节点的左节点
        root->left = trimBST(root->left, low, high);
        // 修剪右子树，并将更新后的树的根节点作为root节点的右节点
        root->right = trimBST(root->right, low, high);

        return root;
    }
};