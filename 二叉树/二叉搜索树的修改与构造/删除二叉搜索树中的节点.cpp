/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 获取当前二叉搜索树最左边的节点
    TreeNode* getMinNode(TreeNode *root) {
        TreeNode *pre = NULL;
        TreeNode *cur = root;

        while(cur) {
            pre = cur;
            cur = cur->left;
        }
        return pre;
    }

    // 寻找值为key的节点，并且将对应节点删除后便返回更新后的二叉搜索树的根节点
    TreeNode* deleteNode(TreeNode* root, int key) {
        if(root == NULL) return NULL;
        // root节点的值等于key
        if(root->val == key) {
            // root节点为叶子节点，则删除完毕后只需要向上层返回NULL
            if(!root->left && !root->right) return NULL;
            // root节点只有左孩子
            if(root->left && !root->right) return root->left;
            // root节点只有右孩子
            if(!root->left && root->right) return root->right;

            // root节点同时拥有左右孩子，首先获取root节点右子树中值最小的节点
            TreeNode *rightMinNode = getMinNode(root->right);
            // 首先将值最小的节点从右子树中断链，随后使用值最小的节点代替root节点
            rightMinNode->right = deleteNode(root->right, rightMinNode->val);
            rightMinNode->left = root->left;
            return rightMinNode;
        }
        else if(key < root->val)
            // 在左子树中删除置为key的节点，并且将新的左子树根节点作为root节点的左孩子
            root->left = deleteNode(root->left, key);
        else
            // 在右子树中删除置为key的节点，并且将新的右子树根节点作为root节点的右孩子
            root->right = deleteNode(root->right, key);

        return root;
    }
};
