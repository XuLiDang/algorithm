/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 根据当前的整数序列构建二叉树，并且根节点为序列中间的元素。左闭右开
    TreeNode* traversal(vector<int>& nums, int begin, int end) {
        // 序列为空则返回NULL
        if(begin == end) return NULL;

        // 中间元素的下标
        int mid = (begin + end) / 2;
        // 创建节点
        TreeNode *root = new TreeNode(nums[mid]);

        // 序列只有一个元素
        if(end - begin == 1) return root;

        // 切割数组
        int leftBegin = begin;
        int leftEnd = mid;
        int rightBegin = mid + 1;
        int rightEnd = end;

        // 构建左子树，并将左子树的根节点作为当前节点的左孩子
        root->left = traversal(nums, leftBegin, leftEnd);
        // 构建右子树，并将右子树的根节点作为当前节点的左孩子
        root->right = traversal(nums, rightBegin, rightEnd);
        return root;
    }

    // 根据序列构造二叉树的核心思想: 首先寻找分割点，并且将分割点作为当前节点，然后递归处理左区间和右区间
    TreeNode* sortedArrayToBST(vector<int>& nums) {
        return traversal(nums, 0, nums.size());
    }   
};