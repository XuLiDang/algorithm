/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 叶子节点本身无法判断自己是否为左叶子节点，因此需要通过其父节点来判断
    int sumOfLeftLeaves(TreeNode* root) {
        if(root == NULL) return 0;
        // 当前节点为叶子节点则直接返回0
        if(root->left == NULL && root->right == NULL) return 0;

        // 获取左子树中左叶子节点值的总和
        int leftVal = sumOfLeftLeaves(root->left);
        // 若左孩子刚好为叶子节点
        if(root->left && !root->left->left && !root->left->right)
            leftVal = root->left->val;

        // 获取右子树中左叶子节点值的总和
        int rightVal = sumOfLeftLeaves(root->right);
        // 计算当前这棵树的左叶子节点值总和
        int sum = leftVal + rightVal;
        return sum;
    }

    // 迭代法
    int sumOfLeftLeaves(TreeNode* root) {
        if(root == NULL) return 0;
        queue<TreeNode*> q;
        q.push(root);
        int sum = 0;

        while(!q.empty()) {
            // 获得当前层的节点数
            int size = q.size();
            // 遍历当前层的所有节点
            while(size--) {
                TreeNode *cur = q.front();
                q.pop();
                // 当前节点的左孩子是叶子节点
                if(cur->left && !cur->left->left && !cur->left->right)
                    sum += cur->left->val;
                // 将左孩子入队(非空)
                if(cur->left != NULL)
                    q.push(cur->left);
                // 将右孩子入队(非空)
                if(cur->right != NULL)
                    q.push(cur->right);
            }
        }
        return sum;
    }
};