/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    void traversal(TreeNode* root, int &targetSum, int &sum, vector<int> &path, vector<vector<int>> &result) {
        // 当前节点为空，则直接返回
        if(root == NULL) return;

         // 当前节点为叶子节点
        if(root->left == NULL && root->right == NULL) {
            // 加上当前叶子节点的值
            sum += root->val;
            // 将当前叶子节点的值加入路径数组中
            path.push_back(root->val);
            // 查看根节点到该叶子节点的路径是否等于给定目标和的路径
            if(sum == targetSum) {
                // 将当前符合要求的路径加入结果数组中
                result.push_back(path);
                // 回溯 sum 的值
                sum -= root->val;
                // 回溯 path 路径数组
                path.pop_back();
               
            }
            else {
                // 回溯 sum 的值
                sum -= root->val;
                // 回溯 path 路径数组
                path.pop_back(); 
            }
        }

        // 左孩子不为空时才往下遍历左子树，查看是否有等于给定目标和的路径 
        if(root->left) {
            // 加上当前节点的值
            sum += root->val;
            // 将当前节点的值加入路径数组中
            path.push_back(root->val);
            traversal(root->left, targetSum, sum, path, result);
            // 回溯 sum 的值
            sum -= root->val;
            // 回溯 path 路径数组
            path.pop_back(); 

        }
         // 右孩子不为空时才往下遍历右子树，查看是否有等于给定目标和的路径 
        if(root->right) {
            // 加上当前节点的值
            sum += root->val;
            // 将当前节点的值加入路径数组中
            path.push_back(root->val);
            traversal(root->right, targetSum, sum, path, result);
            // 回溯 sum 的值
            sum -= root->val;
            // 回溯 path 路径数组
            path.pop_back();    
        }

    }

    vector<vector<int>> pathSum(TreeNode* root, int targetSum) {
        int sum = 0;
        vector<int> path;
        vector<vector<int>> result;
        traversal(root, targetSum, sum, path, result);
        return result;
    }
};