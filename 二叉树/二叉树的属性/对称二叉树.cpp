/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 判断 left 和 right 两个节点代表的子树是否相互对称
    bool compare(TreeNode *left, TreeNode *right) {
        // 左节点与右节点均为空直接返回true
        if(left == NULL && right == NULL) return true;
        // 左右任有一个空一个不空则直接返回false
        if(left != NULL && right == NULL) return false;
        if(left == NULL && right != NULL) return false;
        // 左右节点不相等则直接返回false
        if(left->val != right->val) return false;

        // 继续往下判断left节点的右子树是否与right节点的左子树对称
        bool outside = compare(left->right, right->left);
        // 继续往下判断left节点的左子树是否与right节点的右子树对称
        bool inside = compare(left->left, right->right);

        // 前面的条件均满足，此时可以说明 left 和 right 两个节点代表的子树相互对称
        return outside && inside;

    }

    bool isSymmetric1(TreeNode* root) {
        if (root == NULL) return true;
        return compare(root->left, root->right);
    }

    bool isSymmetric2(TreeNode* root) {
       queue<TreeNode*> que;
       que.push(root->left);
       que.push(root->right);

        // 接下来就要判断这根节点的左右子树是否相互翻转
        while(!que.empty()) {
            // 获取队列的前两个节点，这两个节点分别为左右节点
            TreeNode* leftNode = que.front(); que.pop();
            TreeNode* rightNode = que.front(); que.pop();

            // 左节点为空、右节点为空，此时说明是对称的
            if(!leftNode && !rightNode) continue;
            // 左右一个节点不为空，或者都不为空但数值不相同，返回false
            if ((!leftNode || !rightNode || (leftNode->val != rightNode->val))) 
                return false;
            
            // 加入左节点右孩子
            que.push(leftNode->right); 
            // 加入右节点左孩子
            que.push(rightNode->left);
            // 加入左节点左孩子
            que.push(leftNode->left);
            // 加入右节点右孩子
            que.push(rightNode->right);
       }

       return true;
    }
};