/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    void traversal(TreeNode* root, string path, vector<string> &result) {
        // 当前节点为空则直接返回
        if(root == NULL) return;
        // 当前节点为叶子节点，此时需要将节点值加入当前路径，并将路径加入结果数组
        if(root->left == NULL && root->right == NULL) {
            path += to_string(root->val);
            result.push_back(path);
        }
        // 非叶子节点
        path += to_string(root->val);
        path += "->";
        // 遍历左子树
        traversal(root->left, path, result);
        // 当遍历完左子树后，path 参数并不会被改变。这是因为我们进行的是值传递
        // 通过值传递便可实现 path 参数的回溯。
        traversal(root->right, path, result);
    }

    vector<string> binaryTreePaths(TreeNode* root) {
        vector<string> result;
        traversal(root, "", result);
        return result;
    }
};