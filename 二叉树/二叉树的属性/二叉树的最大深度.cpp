/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int depth(TreeNode* root) {
        if(root == NULL) return 0;
        // 获取左子树的深度
        int leftLen = depth(root->left);
        // 获取右子树的深度
        int rightLen = depth(root->right);

        // 比较左右子树的深度，默认左子树较深
        if(leftLen < rightLen)
            leftLen = rightLen;

        // 返回已当前节点为根节点的子树深度
        return leftLen + 1;
    }

    int maxDepth1(TreeNode* root) {
        return depth(root);
    }

    int maxDepth2(TreeNode* root) {
        if (root == NULL) return 0;
        queue<TreeNode*> que;
        que.push(root);
        int depth = 0;

        // 采用层序遍历获取二叉树最大深度
        while(!que.empty()) {
            // 记录深度
            depth++;
            int size = que.size();

            // 遍历当前层的所有节点
            while(size--) {
                TreeNode *cur = que.front();
                que.pop();
                // 将下一层节点入队(不为空)
                if(cur->left)   que.push(cur->left);
                if(cur->right)   que.push(cur->right);
            }
        }
        return depth;
    }
};