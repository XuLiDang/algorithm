/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool traversal(TreeNode* root, int &targetSum, int pathSum) {
        // 当前节点为空，则直接返回false
        if(root == NULL) return false;
        // 当前节点为叶子节点
        if(root->left == NULL && root->right == NULL) {
            //  加上当前叶子节点的值
            pathSum += root->val;
            // 查看根节点到该叶子节点的路径是否等于给定目标和的路径
            if(pathSum == targetSum) 
                // 等于则返回true
                return true;
            else
                // 否则返回false
                return false;
        }

        pathSum += root->val;
        // 往下遍历左子树，查看是否有等于给定目标和的路径，这里我们采用的是值传递
        // 因此函数返回时，pathSum 的值仍然保持不变。
        bool leftSum = traversal(root->left, targetSum, pathSum);
        // 往下遍历右子树，查看是否有等于给定目标和的路径
        bool rightSum = traversal(root->right, targetSum, pathSum);
        // 只要有一条路径符合都返回true
        return leftSum || rightSum;
    }

    bool traversal2(TreeNode* root, int &targetSum, int &pathSum) {
        // 当前节点为空，则直接返回false
        if(root == NULL) return false;

        // 当前节点为叶子节点
        if(root->left == NULL && root->right == NULL) {
            //  加上当前叶子节点的值
            pathSum += root->val;
            // 查看根节点到该叶子节点的路径是否等于给定目标和的路径
            if(pathSum == targetSum) {
                // 回溯 pathSum 的值
                pathSum -= root->val;
                // 等于则返回true
                return true;
            }            
            else {
                // 回溯 pathSum 的值
                pathSum -= root->val;
                // 否则返回false
                return false;
            }              
        }

        bool leftSum = false, rightSum = false;
        // 左孩子不为空时才往下遍历左子树，查看是否有等于给定目标和的路径  
        if(root->left) {
            pathSum += root->val;
            leftSum = traversal(root->left, targetSum, pathSum);
            // 回溯 pathSum 的值
            pathSum -= root->val;   
        }
        // 右孩子不为空时才往下遍历右子树，查看是否有等于给定目标和的路径  
        if(root->right) {
            pathSum += root->val;
            rightSum = traversal(root->right, targetSum, pathSum);
            // 回溯 pathSum 的值
            pathSum -= root->val;   
        }   
        // 只要有一条路径符合都返回true
        return leftSum || rightSum;
    }


    bool hasPathSum(TreeNode* root, int targetSum) {
        int pathSum = 0;
        return traversal(root, targetSum, pathSum);
    }
};