/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 自底向上获取树的高度，同时判断当前这棵树是否为平衡二叉树。而常规做法是首先判断左右子树
    // 是否均为平衡二叉树，随后再分别求两棵子树的高度差，最后才能判断当前子树是否为平衡二叉树
    int getHeight(TreeNode* root) {
        if(root == NULL) return 0;

        // 获取左子树的高度，不为-1则代表左子树为平衡二叉树
        int leftHeight = getHeight(root->left);
        // 高度为-1则代表左子树不是平衡二叉树，因此这棵树也不可能是平衡二叉树
        if(leftHeight == -1) return -1;

        // 获取右子树的高度，不为-1则代表右子树为平衡二叉树
        int rightHeight = getHeight(root->right);
        // 高度为-1则代表右子树不是平衡二叉树，因此这棵树也不可能是平衡二叉树
        if(rightHeight == -1) return -1;

        // 判断左右子树的高度差是否大于1，大于1代表当前这棵树不是平衡二叉树
        if(abs(leftHeight - rightHeight) > 1)
            return -1;
        // 左右子树的高度差小于1则代表当前这棵树为平衡二叉树
        // 并返回当前这棵树的高度
        else {
            if(leftHeight < rightHeight)
                leftHeight = rightHeight;
            return 1 + leftHeight;
        }
                     
    }

    // 思路：一棵树若为平衡二叉树，则左右子树均为平衡二叉树，且两者高度差小于1
    bool isBalanced(TreeNode* root) {
        if(getHeight(root) != -1)
            return true;
        else
            return false;
    }
};