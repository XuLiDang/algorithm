/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 记录前一个节点的数值
    int pre = 0;

    // 采用左中右顺序遍历二叉搜索树得到的是一个递增序列，那么采用右中左顺序所得到的则是一个递减序列
    // 因此，根据这个特性，我们就可以从大到小来更新二叉搜索树中的节点值
    void traversal(TreeNode* root) {
        if(root == NULL) return;
        // 先遍历右子树
        traversal(root->right);
        // 更新当前节点的数值
        root->val += pre;
        // 将当前节点的数值作为前一个节点数值。按照这种方法，第一个节点的值为自己本身的值
        // 而第二个节点的值则是第一个节点的值 + 第二个节点的值，后面的情况以此类推。
        pre = root->val;
        // 遍历左子树
        traversal(root->left);
    }

    // 将二叉搜索树中的每个节点的值替换成树中大于或者等于该节点值的所有节点值之和。
    TreeNode* convertBST(TreeNode* root) {
        traversal(root);
        return root;
    }
};