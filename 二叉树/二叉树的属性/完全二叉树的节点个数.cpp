/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int countNodes1(TreeNode* root) {
        if(root == NULL) return 0;
        queue<TreeNode*> que;
        int count = 0;
        que.push(root);

        // 采用层序遍历获取完全二叉树的节点个数
        while(!que.empty()) {
            // 获得当前层的节点数
            int size = que.size();
             // 遍历当前层的节点
            while(size--) {
                // 获得队头节点
                TreeNode *cur = que.front();
                // 队头节点出队
                que.pop();
                // 节点数加一
                count++;
                // 将当前节点的左孩子入队
                if(cur->left != NULL)
                    que.push(cur->left);
                // 将当前节点的右孩子入队
                if(cur->right != NULL)
                    que.push(cur->right);
            }
        }
        // 返回节点数
        return count;
    }

    // 思路：首先查看当前这棵树是否为满二叉树，是的话则按照公式计算出当前这棵树的节点数
    // 若不是则递归调用countNodes函数分别求出左右子树的节点数，随后再加上1
    int countNodes2(TreeNode* root) {
        if(root == NULL) return 0;
        TreeNode *l, *r;
        l = root; r = root;
        int lLen = 0, rLen = 0;

        // 从左侧计算树的高度
        while(l) {
            lLen++;
            l = l->left;
        }

        // 从右侧计算树的高度
        while(r) {
            rLen++;
            r = r->right;
        }

        // 如果左右侧计算的高度相同，则是一棵满二叉树
        if(lLen == rLen)
            return pow(2, lLen) - 1

        // 如果左右侧的高度不同，则按照普通二叉树的逻辑计算
        return 1 + countNodes(root->left) + countNodes(root->right);

    }
};