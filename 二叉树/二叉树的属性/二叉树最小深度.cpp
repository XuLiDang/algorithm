/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int getMinDepth(TreeNode* root) {
        if(root == NULL) return 0;

        // 左孩子不空而右孩子为空
        if(root->left != NULL && root->right == NULL) 
            return 1 + getMinDepth(root->left);
        // 左孩子为空而右孩子不空
        if(root->left == NULL && root->right != NULL) 
            return 1 + getMinDepth(root->right);

        // 左右孩子均为空或均不空
        int ldepth = getMinDepth(root->left);
        int rdepth = getMinDepth(root->right);

        // 指定ldepth为深度较小的一方
        if(ldepth > rdepth)
            ldepth = rdepth;

        return ldepth + 1;
    }

    int minDepth1(TreeNode* root) {
        if(root == NULL) return 0;

        return getMinDepth(root);
    }

    int minDepth2(TreeNode* root) {
        if(root == NULL) return 0;
        queue<TreeNode*> que;
        que.push(root);
        int depth = 0;

        while(!que.empty()) {
            // 树的深度加1
            depth++;
            // 获得当前层的节点数
            int size = que.size();

            // 遍历当前层的节点
            while(size--) {
                TreeNode* cur = que.front(); que.pop();
                // 队头节点的左右孩子均为空，则代表已找到最小深度
                if(!cur->left && !cur->right)
                    return depth;
                // 将队头节点的左孩子入队(不为空)
                if(cur->left)
                    que.push(cur->left);
                // 将队头节点的右孩子入队(不为空)
                if(cur->right)
                    que.push(cur->right);
            }
        }

        return 0;
    }
};