/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 采用层序遍历，只需记录最后一层的第一个节点的值即可
    int findBottomLeftValue(TreeNode* root) {
        if(root == NULL) return 0;
        queue<TreeNode*> q;
        int result = 0;
        q.push(root);

        while(!q.empty()) {
            // 获得当前层的节点数
            int size = q.size();
             // 遍历当前层的所有节点
            for(int i = 0; i < size; i++) {
                TreeNode* cur = q.front();
                q.pop();
                // 记录每层第一个节点的值
                if(i == 0) result = cur->val;
                 // 将左孩子入队(非空)
                if(cur->left != NULL) q.push(cur->left);
                // 将右孩子入队(非空)
                if(cur->right != NULL) q.push(cur->right);
            }
        }

        return result;
    }
};