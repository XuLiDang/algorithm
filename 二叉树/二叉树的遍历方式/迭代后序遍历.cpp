class Solution {
public:
    // 迭代后序遍历的结果可对前序遍历的结果进行加工后得到
    // 如前序遍历的输出顺序是中左右，那么首先将其变为中右左
    // 最后再对结果进行翻转，即中右左翻转后得到的就是左右中
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> result;
        stack<TreeNode*> st;
        if (root == NULL) return result;

        // 首先将根节点入栈
        st.push(root);
        while(!st.empty()) {
            // 获取栈顶的节点
            TreeNode *cur = st.top();
            // 将栈顶节点的值加入结果数组中
            result.push_back(cur->val);
            // 栈顶节点出栈
            st.pop();

            // 然后将栈顶的节点的左孩子入栈(不为空)
            if(cur->left != NULL)
                st.push(cur->left);      
            // 先将栈顶的节点的右孩子入栈(不为空)
            if(cur->right != NULL)
                st.push(cur->right);               
        }

        // 翻转结果数组
        int left = 0, right = result.size() - 1;
        while(left < right) {
            swap(result[left], result[right]);
            left++;
            right--;
        }

        return result;
    }
};