/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> preorderTraversal(TreeNode* root) {
        vector<int> result;
        stack<TreeNode*> st;
        if (root == NULL) return result;

        // 首先将根节点入栈
        st.push(root);
        while(!st.empty()) {
            // 获取栈顶的节点
            TreeNode *cur = st.top();
            // 将栈顶节点的值加入结果数组中
            result.push_back(cur->val);
            // 栈顶节点出栈
            st.pop();
            // 先将栈顶的节点的右孩子入栈(不为空)
            if(cur->right != NULL)
                st.push(cur->right);
            // 然后将栈顶的节点的右左孩子入栈(不为空)
            if(cur->left != NULL)
                st.push(cur->left);            
        }
        return result;
    }
};