/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        queue<TreeNode*> que;
        vector<vector<int>> result;
        if (root != NULL) que.push(root);

        // 遍历队列元素，直至队列为空
        while(!que.empty()) {
            // 获得当前层的节点数
            int size = que.size();
            // tmp数组用于暂存每一层的遍历结果
            vector<int> tmp;

            // 遍历当前层的所有节点
            while(size) {
                size --;
                // 将队头节点出队，并将节点的值插入到tmp数组中
                TreeNode *cur = que.front();
                tmp.push_back(cur->val);
                que.pop();
                // 将队头节点的左右孩子入队(若不为空)
                if(cur->left) que.push(cur->left);
                if(cur->right) que.push(cur->right);
            }
            // 将当前层的遍历结果放入result数组
            result.push_back(tmp);
        }

        return result;
    }
};