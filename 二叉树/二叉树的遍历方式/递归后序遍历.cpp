class Solution {
public:
    void traversal(vector<int> &result, TreeNode *root) {
        if(root == NULL) return;

        traversal(result, root->left);
        traversal(result, root->right);
        result.push_back(root->val);
    }

    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> result;
        traversal(result, root);
        return result;
    }
};