/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> result;
        stack<TreeNode*> st;
        TreeNode* cur = root;

        while(!st.empty() || cur != NULL) {
            // 当前节点不为空则继续往左子树方向走
            if(cur != NULL) {
                st.push(cur);
                cur = cur->left; // 左
            }
            // 当前节点为空则需要回退到父节点，即栈顶元素
            else {
                cur = st.top();
                result.push_back(cur->val); // 中
                st.pop();
                // 将右孩子作为当前节点
                cur = cur->right; // 右
            }
        }

    }
};