// 创建大根堆，对heap数组[low,high]范围进行向下调整
// low为欲调整结点下标，high为堆最后一个元素下标
void downAdjust1(int low, int high, int *heap) {
	// i为欲调整结点的下标，j则为该结点的左孩子
	int i = low;
	int j = 2 * i;
	// 当前欲调整节点存在左孩子
	while(j <= high) {
		// 当前欲调整节点存在右孩子且右孩子的值大于左孩子
		if(j + 1 <= high && heap[j] < heap[j + 1]) 
			j = j + 1;
		 //如果孩子中最大的权值比欲调整结点大
		if(heap[j] > heap[i]) {
			//交换最大权值孩子和欲调整结点
			swap(heap[i],heap[j]);
			// 更新欲调整节点
			i = j;
			// j更新为欲调整节点的左孩子
			j = 2 * i;
		}
		else
			// 孩子中权值均比欲调整结点i小，调整结束
			break;
	}
}

// 创建小根堆，对heap数组[low,high]范围进行向下调整
// low为欲调整结点下标，high为堆最后一个元素下标
void downAdjust2(int low, int high, int *heap) {
	// i为欲调整结点的下标，j则为该结点的左孩子
	int i = low;
	int j = 2 * i;
	// 当前欲调整节点存在左孩子
	while(j <= high) {
		// 当前欲调整节点存在右孩子且右孩子的值小于左孩子
		if(j + 1 <= high && heap[j] > heap[j + 1]) 
			j = j + 1;
		 //如果孩子中最小的权值比欲调整结点小
		if(heap[j] > heap[i]) {
			//交换最小权值孩子和欲调整结点
			swap(heap[i],heap[j]);
			// 更新欲调整节点
			i = j;
			// j更新为欲调整节点的左孩子
			j = 2 * i;
		}
		else
			// 孩子中权值均比欲调整结点i大，调整结束
			break;
	}
}

void createHeap(int *heap, int n) {
	// 序列元素个数为n的非叶子结点为[1,n/2], n/2向下取整
	// 每次调整完一个节点后，以该节点为根节点的子树中，值最大的节点便是根节点。
	for(int i = n / 2; i >= 1; i--) {
		downAdjust(i, n, heap);
	}
}

void deleteHeap(int *heap, int n) {
	while(n > 1) {
		n--;
		heap[1] = heap[n];
		downAdjust1(1,n,heap);	
	}
	heap[1] = 0;	
}

void main() {
	const int MAX = 100;
	// 从下标1开始存储节点
	int heap[MAX + 1];
	int n = heap.size() - 1;

	createHeap(heap, n);
	deleteHeap(heap,n);

}