class Solution {
public:
    void backtracking(vector<int>& nums, vector<vector<int>>& result, vector<int>& path, int startIndex) {
        result.push_back(path);
        if(startIndex >= nums.size()) return;

        for(int i = startIndex; i < nums.size(); i++) {
            // 加入当前数字，形成新的子集
            path.push_back(nums[i]);
            // 从 i + 1 开始查找子集
            backtracking(nums, result, path, i + 1);
            // 回溯
            path.pop_back();
        }
    }

    vector<vector<int>> subsets(vector<int>& nums) {
        vector<vector<int>> result;
        vector<int> path;
        backtracking(nums, result, path, 0);
        return result;
    }
};