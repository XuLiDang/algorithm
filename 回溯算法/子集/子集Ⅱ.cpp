class Solution {
public:
    void backtracking(vector<int>& nums, vector<int>& path, vector<vector<int>>& result, int startIndex) {
        result.push_back(path);
        if(startIndex >= nums.size()) return;

        for(int i = startIndex; i < nums.size(); i++) {
            // 同一层的上一根树枝使用的数字值与当前数字相同，为避免出现相同子集的情况，这里直接跳过该数字
            if(i > startIndex && nums[i] == nums[i - 1]) continue;
            // 加入当前数字，形成新的子集
            path.push_back(nums[i]);
            // 从 i + 1 开始查找子集
            backtracking(nums, path, result, i + 1);
            // 回溯
            path.pop_back();
        }
    }

    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
        vector<int> path;
        vector<vector<int>> result;
        // 先把 nums 数组进行排序，使得相同的元素都挨在一起
        sort(nums.begin(), nums.end());
        backtracking(nums, path, result, 0);
        return result;
    }
};