class Solution {
public:
    vector<int> path;
    vector<vector<int>> result; 

    void backtracking(vector<int>& nums, vector<int>& used) {
        // path 的大小等于数组的大小
        if(path.size() == nums.size()) {
            result.push_back(path);
            return;
        }

        // 每层都是从 0 开始搜索而不是 startIndex，因为一个排列里一个元素只
        // 能使用一次，因此需要一个 used 数组记录当前 path 里都放了哪些元素，
        for(int i = 0; i < nums.size(); i++) {
            // 当前元素已经存在当前 path，此时需要跳过该元素
            if(used[i] == 1) continue;
            path.push_back(nums[i]);
            used[i] = 1;
            backtracking(nums, used);
            used[i] = 0;
            path.pop_back();
        }
    }

    // 注意本题的nums数组是不含重复数字的，因此不要考虑同一层出现相同数字的情况
    vector<vector<int>> permute(vector<int>& nums) {
        vector<int> used(nums.size(), 0);
        backtracking(nums, used);
        return result;
    }
};