class Solution {
public:
    vector<int> path;
    vector<vector<int>> result; 

    void backtracking(vector<int>& nums, vector<int>& used) {
        // path 的大小等于数组的大小，此时 path 中的数字已经是全排列
        if(path.size() == nums.size()) {
            result.push_back(path);
            return;
        }

        // 每层都是从 0 开始搜索而不是 startIndex，因为一个排列里一个元素只
        // 能使用一次，因此需要一个 used 数组记录当前 path 里都放了哪些元素。
        // 除此之外，我们还需要判断本层中已经使用了哪些元素，并且跳过这些已经
        // 使用过的元素，从而避免出现重复的全排列 
        for(int i = 0; i < nums.size(); i++) {
            // 当前元素已经存在当前 path，此时需要跳过该元素
            if(used[i] == 1) continue;
            // 当前元素已经在本层使用过，此时需要跳过该元素，避免出现重复的全排列
            if(i > 0 && nums[i] == nums[i - 1]  && used[i - 1] == 0) continue;
        
            path.push_back(nums[i]);
            used[i] = 1;
            backtracking(nums, used);
            used[i] = 0;
            path.pop_back();
        }
    }

    // 注意本题的nums数组是包含重复数字的，因此需要考虑同一层出现相同数字的情况
    vector<vector<int>> permuteUnique(vector<int>& nums) {
        vector<int> used(nums.size(), 0);
        // 对nums数组排序，使得大小相同的数字挨在一起
        sort(nums.begin(), nums.end()); 
        backtracking(nums, used);
        return result;
    }
};