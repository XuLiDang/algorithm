class Solution {
private:
// unordered_map<出发机场, map<到达机场, 航班次数>> targets
unordered_map<string, map<string, int>> targets;
// ticketNum 代表有多少个航班
bool backtracking(int ticketNum, vector<string>& result) {
    // result中存放的是行程中包含的机场(机场可重复)
    if (result.size() == ticketNum + 1) {
        return true;
    }
    for (pair<const string, int>& target : targets[result[result.size() - 1]]) {
        // 记录到达机场是否飞过了
        if (target.second > 0 ) { 
            result.push_back(target.first);
            target.second--;
            if (backtracking(ticketNum, result)) return true;
            result.pop_back();
            target.second++;
        }
    }
    return false;
}
public:
    vector<string> findItinerary(vector<vector<string>>& tickets) {
        vector<string> result;
        for (const vector<string>& vec : tickets) {
            // 在target中记录映射关系 <出发机场, map<到达机场, 航班次数>>
            // vec[0] 出发机场，vec[1] 到达机场
            targets[vec[0]][vec[1]]++; 
        }
        // 起始机场
        result.push_back("JFK"); 
        backtracking(tickets.size(), result);
        return result;
    }
};