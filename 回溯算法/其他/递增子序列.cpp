class Solution {
public:
    void backtracking(vector<int>& nums, vector<int>& path, vector<vector<int>>& result, int startIndex) {
        // 递增序列的元素个数大于等于2
        if(path.size() > 1)
            result.push_back(path);

        if(startIndex >= nums.size())
            return;

        // 使用set对本层元素进行去重
        unordered_set<int> uset;
        for(int i = startIndex; i < nums.size(); i++) {
            // 递增序列不为空，且当前元素值小于递增序列最后一个元素的值，则直接跳过当前元素
            if(!path.empty() && nums[i] < path.back()) continue;
            // 当前元素在本层已经使用过，为防止出现重复的递增序列，需要直接跳过当前元素
            if(uset.find(nums[i]) != uset.end()) continue;
            // 记录这个元素在本层用过了，本层后面不能再用了
            uset.insert(nums[i]);
            // 更新递增序列
            path.push_back(nums[i]);
            // 从 i + 1 开始更新递增序列
            backtracking(nums, path, result, i + 1);
            // 回溯
            path.pop_back();
        }
    }    

    // 注意该题要求查找的是原数组中的递增序列，因此不能根据排序后的数组来获取递增序列
    vector<vector<int>> findSubsequences(vector<int>& nums) {
        vector<vector<int>> result;
        vector<int> path;      
        backtracking(nums, path, result, 0);
        return result;
    }
};