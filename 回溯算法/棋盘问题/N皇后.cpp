class Solution {
public:
    vector<vector<string>> result;
    bool isValid(int row, int col, vector<string>& chessboard, int n) {
        // 检查列，这里只需要判断 row 之前的行是否有皇后
        for (int i = 0; i < row; i++) { 
            if (chessboard[i][col] == 'Q') {
                return false;
            }
        }
        // 检查 45 度角是否有皇后，这里只需要判断 row 之前的行是否有皇后
        for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; i--, j--) {
            if (chessboard[i][j] == 'Q') {
                return false;
            }
        }
        // 检查 135 度角是否有皇后，这里只需要判断 row 之前的行是否有皇后
        for(int i = row - 1, j = col + 1; i >= 0 && j < n; i--, j++) {
            if (chessboard[i][j] == 'Q') {
                return false;
            }
        }
        return true;
    }

    // n 为输入的棋盘大小，row 是棋盘的第几行的下标
    void backtracking(int n, int row, vector<string>& chessboard) {
        if(row == n) {
            result.push_back(chessboard);
            return;
        }

        for(int col = 0; col < n; col++) {
            // 验证是否可以在 chessboard[row][col] 中放置皇后
            if(!isValid(row, col, chessboard, n)) continue;
            // 放置皇后
            chessboard[row][col] = 'Q';
            // 继续往下放置皇后
            backtracking(n, row + 1, chessboard);
            // 撤销皇后
            chessboard[row][col] = '.';
        }
    }

    vector<vector<string>> solveNQueens(int n) {
        // 用 chessboard 来模拟棋盘，每个string元素代表棋盘一行的内容
        // 首先将棋盘的每个位置都放置 '.'
        vector<string> chessboard(n, string(n, '.'));
        backtracking(n, 0, chessboard);
        return result;
    }
};