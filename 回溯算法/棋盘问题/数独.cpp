class Solution {
public:
    bool isValid(int row, int col, char val, vector<vector<char>>& board) {
        // 检查同一行是否有相同的元素
        for(int i = 0; i < board[0].size(); i++) {
            if(board[row][i] == val) 
                return false;
        }

        // 检查同一列是否有相同的元素
        for(int i = 0; i < board.size(); i++) {
            if(board[i][col] == val) 
                return false;
        }

        // 检查九宫格是否有重复的数字
        int startRow = (row / 3) * 3;
        int startCol = (col / 3) * 3;
        for(int i = startRow; i < startRow + 3; i++) 
            for(int j = startCol; j < startCol + 3; j++) {
                if(board[i][j] == val)
                    return false;
            }

        return true;
    }

    bool backtracking2(vector<vector<char>>& board, int startRow, int startCol) {
        // 遍历行
        for(int i = startRow; i < board.size(); i++) {
            // 遍历列
            for(int j = startCol; j < board[0].size(); j++) {
                // [i][j] 位置上的值不是 '.' 则直接跳过
                if(board[i][j] != '.') continue;

                for(char k = '1'; k <= '9'; k++) {
                    // 判断是否可以在 [i][j] 位置上放 k
                    if(!isValid(i, j, k, board)) continue;
                    // 在 [i][j] 位置上放 k
                    board[i][j] = k;

                    // 下次递归无需从头检查所有位置的数字，可以直接从 i 行 j + 1 列或者 i + 1 行 0 列检查
                    if(j == board[0].size() - 1) {
                        // 递归往矩阵上放置数字，找到一组满足条件的解决方案时返回 true
                        if (backtracking(board, i + 1, 0)) return true; 
                    }
                    else {
                        // 递归往矩阵上放置数字，找到一组满足条件的解决方案时返回 true
                        if (backtracking(board, i, j + 1)) return true; 
                    }
                    
                    // 回溯
                    board[i][j] = '.';
                }
                // 尝试了9个数字都无法找到符合要求的方案，此时需要返回false
                return false;
            }
        }
        // 代码执行到这里则代表找到了一组符合条件的解决方案，此时返回true
        return true; 
    }


    bool backtracking(vector<vector<char>>& board) {
        // 遍历行
        for(int i = startRow; i < board.size(); i++) {
            // 遍历列
            for(int j = startCol; j < board[0].size(); j++) {
                // [i][j] 位置上的值不是 '.' 则直接跳过
                if(board[i][j] != '.') continue;

                for(char k = '1'; k <= '9'; k++) {
                    // 判断是否可以在 [i][j] 位置上放 k
                    if(!isValid(i, j, k, board)) continue;
                    // 在 [i][j] 位置上放 k
                    board[i][j] = k;
                  
                    // 递归往矩阵上放置数字，找到一组满足条件的解决方案时返回 true
                    if (backtracking(board)) return true; 
                                              
                    // 回溯
                    board[i][j] = '.';
                }
                // 尝试了9个数字都无法找到符合要求的方案，此时需要返回false
                return false;
            }
        }
        // 代码执行到这里则代表找到了一组符合条件的解决方案，此时返回true
        return true; 
    }

    void solveSudoku(vector<vector<char>>& board) {
      backtracking(board);  
    }
};