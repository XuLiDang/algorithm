class Solution {
public:
    // 判断字符串s在左闭又闭区间[start, end]所组成的数字是否合法
    bool isValid(const string& s, int start, int end) {
        if (start > end) {
            return false;
        }
        if (s[start] == '0' && start != end) { // 0开头的数字不合法
                return false;
        }
        int num = 0;
        for (int i = start; i <= end; i++) {
            if (s[i] > '9' || s[i] < '0') { // 遇到非数字字符不合法
                return false;
            }
            num = num * 10 + (s[i] - '0');
            if (num > 255) { // 如果大于255了不合法
                return false;
            }
        }
        return true;
    }

    void backtracking(string& s, vector<string>& result, int startIndex, int pointNum) {
        if(pointNum == 3) {
            // 判断第四段子串是否符合要求
            if(isValid(s, startIndex, s.size() - 1))
                result.push_back(s);
            return;
        }

        for(int i = startIndex; i < s.size(); i++) {
            // 判断子串 [startIndex, i] 是否符合要求，不符合则直接结束本层循环
            if(!isValid(s, startIndex, i)) break;
            // 在子串 [startIndex, i] 后面添加一个'.'
            s.insert(s.begin() + i + 1, '.');
            // '.'数量加1
            pointNum ++;

            // 因此我们往原字符串中添加了一个'.'，因此要从 i + 2 开始切割子串
            backtracking(s, result, i + 2, pointNum);

            // 回溯
            pointNum--;
            // 回溯删除'.'
            s.erase(s.begin() + i + 1);
        }
    }

    vector<string> restoreIpAddresses(string s) {
        // 结果数组
        vector<string> result;
        // 直接切割原字符串
        backtracking(s, result, 0, 0);
        return result;
    }
};