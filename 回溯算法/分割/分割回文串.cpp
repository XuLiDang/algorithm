class Solution {
public:
    // 判断字符串是否为回文字符串
    bool isPalindrome(string &s, int start, int end) {
        while(start <= end) {
            if(s[start] != s[end]) return false;
            start++;
            end--;
        }

        return true;
    }

    void backtracking(vector<string>& path, vector<vector<string>>& result, string& s, int startIndex) {
        // 切割点大于等于字符串的长度，代表切割过程结束
        if(startIndex >= s.size()) {
            result.push_back(path);
            return;
        }

        for(int i = startIndex; i < s.size(); i++) {
            // 判断子串 [startIndex, i] 是否为回文串
            if(isPalindrome(s, startIndex, i)) {
                // 获取子串 [startIndex, i]
                string str = s.substr(startIndex, i - startIndex + 1);
                // 存放回文字符串
                path.push_back(str);
            } else {
                continue;
            }
            // 继续从 [i + 1, s.size() - 1] 中截取子串 
            backtracking(path, result, s, i + 1);
            // 回溯
            path.pop_back();
        }
    }

    vector<vector<string>> partition(string s) {
        // 用于存放回文串
        vector<string> path;
        // 结果数组
        vector<vector<string>> result;
        backtracking(path, result, s, 0);
        return result;
    }
};