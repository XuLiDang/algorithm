class Solution {
public:
    // 记录当前组合中的数字
    vector<int> path;
    // 结果数组
    vector<vector<int>> result;
    void backtracking(vector<int>& candidates, int target, int startIndex, int sum) {
        // 当前组合中数字的和等于target
        if (sum == target) {
            result.push_back(path);
            return;
        }

        // 剪枝优化: 将当前数字加入组合后，若其值大于target，则无需选取该数字及其以后的数字
        // 需要注意的是，我们已经使 candidates 数组变为了一个有序数组，相同的元素都挨在一起
        for(int i = startIndex; i < candidates.size() && sum + candidates[i] <= target; i++) {
            // 同一层的上一根树枝使用的数字值与当前数字相同，为避免出现相同组合的情况，这里直接跳过该数字
            if(i > startIndex && candidates[i - 1] == candidates[i]) continue;
            // 将数字加入当前组合中
            path.push_back(candidates[i]);
            // 更新组合中数字的和
            sum += candidates[i];           

            // i + 1 表明每个数字只能在同一组合中使用一次
            backtracking(candidates, target, i + 1, sum);

            // 回溯
            path.pop_back();
            sum -= candidates[i];
        }


    }
    // 该题的难点在于，给定的集合 candidates 里面包含有重复的元素，但要求不能出现重复的组合
    // 例如[1,1,7]，target = 8。第一个1与7构成的组合跟第二个1与7构成的组成重复了，那么为了
    // 避免这种情况，需要进行特殊处理来实现去重。
    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
        path.clear();
        result.clear();
        // 先把 candidates 数组进行排序，使得相同的元素都挨在一起
        sort(candidates.begin(), candidates.end());
        backtracking(candidates, target, 0, 0);
        return result;
    }
};