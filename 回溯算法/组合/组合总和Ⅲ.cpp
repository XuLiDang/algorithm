class Solution {
public:
    void backtracking(int k, int n, int startIndex, int& sum, vector<vector<int>>& result, vector<int>& path) {
        // 组合中数字的个数等于k
        if(path.size() == k) {
            // 组合中数字的和等于sum
            if(sum == n)
                // 将当前组合中的数字加入结果数组中
                result.push_back(path);
            return;
        }

        // 若for循环选择的起始位置后的元素个数少于我们需要的元素个数，那么就没有必须再继续往下搜索了
        // 另外若当前组合中数字的和已经等于大于n了，而此时数字的个数肯定小于k，那么也无需继续往下搜索了
        for(int i = startIndex; i <= 9 - (k - path.size()) + 1 && sum < n; i++) {
            // 将数字i加入组合
            path.push_back(i);
            // 记录组合中数字的和
            sum += i;
            // 继续往组合里添加数字，直到数字个数为k
            backtracking(k, n, i + 1, sum, result, path);
            // 回溯
            path.pop_back();
            // 回溯
            sum -= i;
        }

    }

    vector<vector<int>> combinationSum3(int k, int n) {
        // 结果数组
        vector<vector<int>> result;
        // 记录组合中的数字
        vector<int> path;
        // 记录组合中数字的和
        int sum = 0;
        backtracking(k, n, 1, sum, result, path);
        return result;
    }   
};