class Solution {
public:
    const string letterMap[10] = {
        "", // 0
        "", // 1
        "abc", // 2
        "def", // 3
        "ghi", // 4
        "jkl", // 5
        "mno", // 6
        "pqrs", // 7
        "tuv", // 8
        "wxyz", // 9
    };

    void backtracking(string& digits, int index, string& path, vector<string>& result) {
        // 字母组合个数等于数字串的长度
        if(path.size() == digits.size()) {
            // 就当前的字母组合放入结果数组中
            result.push_back(path);
            return;
        }

        // 将index指向的数字转为int
        int digit = digits[index] - '0';
        // 获取数字对应的字母串
        string letters = letterMap[digit];
        // 遍历选取数字对应的字母串中的每一个字母
        for(int i = 0; i < letters.size(); i++) {
            path.push_back(letters[i]);
            // 处理下一个数字。index进行的值传递，所以会自动回溯
            backtracking(digits, index + 1, path, result);
            // 回溯
            path.pop_back();
        }

    }

    vector<string> letterCombinations(string digits) {
        // 结果数组
        vector<string> result;
        // 电话号码的长度为0
        if(digits.size() == 0) return result;
        // 字母组合
        string path = "";
        // 电话号码下标
        int index = 0;
        // 数字串的个数决定了递归的深度，而每个数字对应的字母串的个数则决定了每层递归中遍历选取的宽度
        backtracking(digits, index, path, result);
        return result;
    }
};