class Solution {
public:
    void backtracking(int n, int k, int startIndex, vector<int> &path, vector<vector<int>> &result) {
        // 当前组合的长度为k就结束递归
        if(path.size() == k) {
            // 把当前组合存放到结果数组中
            result.push_back(path);
            return;
        }

        // 遍历[startIndex, n]范围内的所有整数
        for(int i = startIndex; i <= n; i++) {
            // 选择其中一个整数加入当前组合
            path.push_back(i);
            // 从[i + 1, n]范围中取 k - path.size() 个整数
            backtracking(n, k, i + 1, path, result);
            // 回溯
            path.pop_back();
        }

        // 遍历[startIndex, n]范围内的所有整数。剪枝操作优化，若for循环选择的起始位置
        // 之后的元素个数少于我们需要的元素个数，那么就没有必须再继续往下搜索了
        for(int i = startIndex; i <= n - (k - path.size()) + 1; i++) {
            // 选择其中一个整数加入当前组合
            path.push_back(i);
            // 从[i + 1, n]范围中取 k - path.size() 个整数
            backtracking(n, k, i + 1, path, result);
            // 回溯
            path.pop_back();
        }

    }

    vector<vector<int>> combine(int n, int k) {
        // 存放当前组合
        vector<int> path;
        // 存放符合要求的所有组合
        vector<vector<int>> result;
        // 从[1, n]范围中取 k - path.size() 个整数
        backtracking(n, k, 1, path, result);
        return result;
    }
};