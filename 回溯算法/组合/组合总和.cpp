class Solution {
public:
    // 数字组合
    vector<int> path;
    // 结果数组
    vector<vector<int>> result;
    void backtracking(vector<int>& candidates, int sum, int target, int startIndex) {
        if(sum == target) {
            result.push_back(path);
            return;
        }

        // 选取candidates数组中的整数，注意[2,3] 与 [3,2]是重复的组合
        // 因此我们需要 startIndex 来排除选取出重复组合的情况
        for(int i = startIndex; i < candidates.size(); i++) {
            // 剪枝操作
            if(sum + candidates[i] > target) continue;
            // 更新组合中的数字
            path.push_back(candidates[i]);
            // 更新组合中数字的总和
            sum += candidates[i];
            // 继续往组合里添加数字，注意按照题目要求，下一层可以重复选取当前的数字
            backtracking(candidates, sum, target, i);
            // 回溯
            path.pop_back();
            // 回溯
            sum -= candidates[i];
        }
    }

    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {   
        // 找出 candidates 中可以使数字和为目标数 target 的所有不同组合
        backtracking(candidates, 0, target, 0);
        return result;
    }
};