class Solution {
public:
    // 贪心算法思想: 遇到账单20时，优先使用10美元进行找零，每次都尽可能地少用5美元来找零
    bool lemonadeChange(vector<int>& bills) {
        int fiveCount = 0, tenCount = 0;

        for(int i = 0; i < bills.size(); i++) {
            // 5 美元的账单
            if(bills[i] == 5)
                fiveCount++;

            // 10 美元的账单
            else if(bills[i] == 10) {
                // 没有 5 美元的零钱肯定无法正确找零，因此直接返回 false
                if(fiveCount == 0)
                    return false;
                // 5 美元的零钱的数量减1，10 美元零钱的数量加1
                else {
                    fiveCount--;
                    tenCount++;
                }
            }

            // 20 美元的账单
            else {
                // 没有 5 美元的零钱肯定无法正确找零，因此直接返回 false
                if(fiveCount == 0)
                    return false;
                // 5 美元 与 10 美元都有
                if(tenCount > 0) {
                    fiveCount--;
                    tenCount--;
                }
                // 只有 5 美元则需要判断是否有 3 张 5 美元的零钱
                else if(fiveCount >= 3) 
                    fiveCount -= 3;
                // 没有 3 张 5 美元的零钱肯定无法正确找零，因此直接返回 false
                else 
                    return false; 
            }

        }

        return true;
    }
};