class Solution {
public:
    // 贪心算法策略: 保证每次买卖股票都能获得利润，从而就能获得最大利润
    // 如果今天买的股票在明天卖出后能挣取利润，则买入今天的股票，并在明天卖出
    // 这样就能保证每次买卖都是挣钱的，因此可以得出最优解
    int maxProfit(vector<int>& prices) {
        int profit = 0;

        for(int i = 0; i < prices.size() - 1; i++) {
            // 明天的价格高于今天的价格
            if(prices[i + 1] > prices[i])
                // 在今天买入并在明天卖出，并用 profit 累计总利润
                profit += prices[i + 1] - prices[i];      
        }

        return profit;
    }
};