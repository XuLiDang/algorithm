class Solution {
public:
    // 思路: 为了满足尽可能多的孩子，就不能浪费饼干，因此用尽量小的饼干去满足小需求的孩子
    // 所以需要对数组进行排序，将小的饼干以及胃口小的孩子排在前面
    int findContentChildren(vector<int>& g, vector<int>& s) {
        // 对饼干和孩子数组进行排序
        sort(g.begin(), g.end());
        sort(s.begin(), s.end());
        // 孩子数组的下标，也表示当前已满足的孩子数
        int child = 0;
        // 饼干数组的下标
        int cookie = 0;

        // 其中一个数组遍历完就退出
        while(child < g.size() && cookie < s.size()) {
            // 当前饼干能满足当前孩子
            if(s[cookie] >= g[child])
                // 已满足的孩子数加1，同时也指向下一个孩子
                child++;
            // 无论饼干是否能满足孩子，只要被吃了就不能再重复使用了
            cookie++;           
        }

        // 返回已满足的孩子数
        return child;
    }
};