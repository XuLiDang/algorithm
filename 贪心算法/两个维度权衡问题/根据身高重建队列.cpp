class Solution {
public:
    // 定义排序时的 < 运算符
    static bool cmp(const vector<int>& a, const vector<int>& b) {
        // a比b高
        if(a[0] > b[0])
            return true;
        // a比b矮
        else if (a[0] < b[0])
            return false;
        // a跟b一样高
        else {
            // k小的排前面
            if(a[1] < b[1])
                return true;
            else
                return false;
        }
    }

    // 解题思路: 1.将数组按照身高从高到矮进行排序，身高相同的则k小的排前面
    // 2. 遍历排序后的数组，优先将身高较高的元素插入队列中下标K的位置，插入
    // 操作过后，每个元素都满足其在队列中的位置要求。因此完成全部插入操作后，
    // 整个所有元素都满足其在队列中的位置要求。
    vector<vector<int>> reconstructQueue(vector<vector<int>>& people) {
        // 将数组按照身高从高到矮进行排序，身高相同的则k小的排前面
        sort(people.begin(), people.end(), cmp);
        // 用于插入元素
        vector<vector<int>> que;
        // 优先按照身高较高的元素的K进行插入操作
        for(int i = 0; i < people.size(); i++) {
            // 将 K 作为当前元素插入的位置
            int position = people[i][1];
            // 将当前元素插入到队列的下标 position
            que.insert(que.begin() + position, people[i]);
        }
        return que;
    }
};