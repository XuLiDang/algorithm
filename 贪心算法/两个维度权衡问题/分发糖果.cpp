class Solution {
public:
    int candy(vector<int>& ratings) {
        // 用于统计每个孩子获得的糖果数，每个孩子至少都能获得一颗糖果
        vector<int> candy(ratings.size(), 1);
        // 从左往右
        // 执行完该循环就能保证，如果当前孩子的评分高于其左边的孩子，那么他的糖果一定比左边的孩子多一颗
        for (int i = 1; i < ratings.size(); i++) {
            if (ratings[i] > ratings[i - 1])
                candy[i] = candy[i - 1] + 1;
        }
        // 从右往左
        // 执行完该循环就能保证，如果当前孩子的评分高于其右边的孩子，那么他的糖果一定比右边的孩子多
        for (int i = ratings.size() - 2; i >= 0; i--) {
            if (ratings[i] > ratings[i + 1] ) {
                candy[i] = max(candy[i], candy[i + 1] + 1);
            }
        }
        // 统计结果
        int result = 0;
        for (int i = 0; i < candy.size(); i++) result += candy[i];
        return result;
    }
};