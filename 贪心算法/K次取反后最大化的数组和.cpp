class Solution {
public:
    int largestSumAfterKNegations(vector<int>& nums, int k) {
        // 对数组进行排序
        sort(nums.begin(), nums.end());
        int sum = 0;
        // 排序后的数组的第一个元素就大于等于0，对于有序正整数序列
        // 只对值最小的元素进行翻转才能使得 数组和 变得最大
        if(nums[0] >= 0) {
            // 只对值最小的元素进行翻转
            while(k > 0){
                nums[0] = -nums[0];
                k--;
            }
            // 求出数组和
            for(int i = 0; i < nums.size(); i++){
                sum += nums[i];
            }
            // 返回数组和
            return sum;
        }
        // 排序后数组的第一个元素小于 0
        else {
            // 对数组中的负数进行翻转，退出循环的情况有以下几种: 1.翻转次数已用完(k = 0) 
            // ，此时数组中可能还是存在负数。2.数组中的元素已全部大于等于0
            for(int i = 0; i < nums.size() && k > 0; i++) {
                if(nums[i] < 0) {
                    nums[i] = -nums[i];
                    k--;
                }
                // 数组中的元素已全部大于等于0时就直接退出循环               
                else break;               
            }
            // 翻转次数已用完，则可直接求出数组和
            if(k == 0) {
                for(int j = 0; j < nums.size(); j++)
                    sum += nums[j];
                // 返回数组和
                return sum;
            }

            // 再次排序数组
            sort(nums.begin(), nums.end());
            // 若K还大于0，则表示数组必定是一个有序正整数序列，因此只对值最小的元素进行翻转
            while(k > 0) {
                nums[0] = -nums[0];
                k--;
            }
            // 求出数组和
            for(int j = 0; j < nums.size(); j++)
                sum += nums[j];
             // 返回数组和
            return sum;
        }

    }
};