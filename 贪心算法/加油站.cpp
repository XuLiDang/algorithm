class Solution {
public:
    // 暴力法，但是会超时
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        // 当前拥有多少升汽油
        int gasCount = 0;
        // 开始时的加油站的索引
        int start;
        // 记录是否已循环过一轮
        int round;

        for(int i = 0; i < gas.size(); i++) {
            // 记录开始时加油站的索引
            start = i;
            // 初始时汽油的升数
            gasCount = gas[i];
            // 表示还未循环过一轮
            round = 0;

            int j = start;
            // 以 start 为起点行驶
            while(j != start || round == 0) {
                // 检查是否可以到达下一个加油站
                if(gasCount - cost[j] < 0)
                    break;

                // 去往下一个加油站
                gasCount = gasCount - cost[j];
                j = (j + 1) % cost.size();
                // 到达加油站后立刻补充汽油
                gasCount += gas[j];
                // j = 0 则代表已循环过一轮
                if(j == 0) 
                    round = 1;
            }

            if(j == start && round == 1)
                return start;
        }

        return -1;
    }

    // 贪心算法思想: 一旦当前剩余油量小于0，则从下一个站点作为出发点，并重新计算当前剩余油量
    int canCompleteCircuit2(vector<int>& gas, vector<int>& cost) {
        // 记录当前剩余油量
        int curSum = 0;
        // 用于记录 cost 和 gas 数组差值的总和
        int totalSum = 0;
        // 初始时将 0 作为出发点
        int start = 0;

        for (int i = 0; i < gas.size(); i++) {
            // 记录当前剩余油量，一旦剩余油量小于0，则代表从[0,i]区间出发后必定会出现断油的情况
            // 因此[0,i]区间都不能作为出发点
            curSum += gas[i] - cost[i];
            totalSum += gas[i] - cost[i];
            // 当前累加rest[i] 和 curSum一旦小于0
            if (curSum < 0) {
                // 起始位置更新为 i + 1   
                start = i + 1;
                // 重新计算当前剩余油量 
                curSum = 0;     
            }
        }

        // 两个数组差值的总和小于0就说明怎么走都不可能跑一圈了
        if (totalSum < 0) 
            return -1; 

        return start;        
    }
};