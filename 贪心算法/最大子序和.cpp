class Solution {
public:
    // 利用贪心算法求解，每次都追求局部最优，从而最后得到整体最优
    // 局部最优: 当前连续和小于0时立刻放弃，并从下一个元素开始计算 "连续和"。
    int maxSubArray(vector<int>& nums) {
        // 用于存放最大连续和
        int result = INT_MIN;
        // 当前连续和
        int count = 0;

        for(int i = 0; i < nums.size(); i++) {
            // 更新当前连续和
            count += nums[i];
            // 更新最大连续和
            if(count > result) 
                result = count;
            // 当前连续和小于0时立刻放弃，并从下一个元素开始重新计算 "连续和"。
            if(count < 0) 
                count = 0; 
        }

        return result;
    }
};