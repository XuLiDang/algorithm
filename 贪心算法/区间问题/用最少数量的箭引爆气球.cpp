class Solution {
public:
    // 定义排序时的 < 运算符
    static bool cmp(const vector<int>& a, const vector<int>& b) {
        if(a[0] < b[0])
            return true;
        else
            return false;
    }


    // 贪心算法思想: 每支箭都引爆尽可能多的气球
    int findMinArrowShots(vector<vector<int>>& points) {
        if (points.size() == 0) return 0;
        // 让气球按照起始位置从小到大地排序，这样起始位置接近的气球就会靠得尽可能的近。
        sort(points.begin(), points.end(), cmp);
        // points 不为空至少需要一支箭
        int arrows = 1;
        // 用于记录当前重叠气球中的最小右边界
        int bound = points[0][1];

        for(int i = 1; i < points.size(); i++) {
            // 当前气球的起始位置 小于或等于 当前重叠气球中的最小右边界
            if(points[i][0] <= bound)
                // 更新当前重叠气球中的最小右边界
                bound = min(bound, points[i][1]);

            // 当前气球的起始位置 大于 当前重叠气球中的最小右边界。说明当前气球与前面的气球不重叠也不相邻
            else {
                // 需要多一支箭
                arrows++;
                // 更新当前重叠气球中的最小右边界
                bound = points[i][1];             
            }
        }

        return arrows;
    }
};