class Solution {
public:
    int jump(vector<int>& nums) {
        // 当前的覆盖范围
        int coverRange = 0;
        // 下一步的覆盖范围 
        int nextCoverRange = 0;
        // 步数
        int steps = 0;

        for(int i = 0; i < nums.size(); i++) {
            // 在当前可覆盖范围内查找下一跳能到达的最远的位置
            nextCoverRange = max(nextCoverRange, i + nums[i]);

            // 已查找到当前覆盖范围的边界
            if(i == coverRange) {
                // 当前覆盖范围的边界并不是数组的最后一个元素
                if(coverRange < nums.size() - 1) {
                    // 往下跳到下一跳能到达的最远的位置，并更新当前可覆盖范围
                    steps++;
                    coverRange = nextCoverRange;
                    // 跳完后发现当前可覆盖范围已包含最后一个元素，则代表跳到最后一个元素了，无需继续往下跳
                    if(coverRange >= nums.size() - 1) break;
                }
                // 当前覆盖范围的边界正好是数组的最后一个元素 
                else
                    // 此时说明已经跳到数组的最后一个元素了，无需继续往下跳
                    break;
            }
        }

        return steps;
    }
};