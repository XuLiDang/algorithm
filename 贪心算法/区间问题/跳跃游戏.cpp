class Solution {
public:
    // 能否达到最后一个下标即可覆盖的下标范围能否到达数组的最后一个下标
    bool canJump(vector<int>& nums) {
        // cover 表示能覆盖到的下标范围为[0, cover]
        int cover = 0;
        // 数组只有一个元素，那么肯定能达到最后一个下标
        if(nums.size() == 1) return true;

        // 在当前可覆盖的范围内移动，每移动一个元素都会尝试更新当前可覆盖的下标范围
        for(int i = 0; i <= cover; i++) {
            // 当前所在位置的下标 + 该位置可以跳跃的最大长度 大于 当前可覆盖的下标范围
            cover = max(cover, i + nums[i]);
            // 覆盖范围可以到达最后一个下标
            if(cover >= nums.size() - 1)
                return true;
        }

        // 已遍历完可覆盖范围内的所有元素，发现当前可覆盖范围还是无法包含数组最后一个元素的下标
        return false;
    }
};