class Solution {
public:
    // 此题的关键点在于要首先确定每个字符最后出现的位置在遍历的
    // 过程中要依赖字符的最后出现位置来更新当前片段右边界的位置
    vector<int> partitionLabels(string s) {
        // 用于存放每个字符最后出现的位置
        unordered_map<char, int> char_index;
        // 结果数组
        vector<int> result;
        // 遍历字符串，在 map 中记录每个字符最后出现的位置
        for(int i = 0; i < s.size(); i++) {
            char_index[s[i]] = i;   
        }

        // 当前片段右边界的位置
        int end = char_index[s[0]];
        // 当前片段的左边界的位置
        int start = 0;
        for(int i = 0; i < s.size(); i++) {
            // 将当前片段右边界的位置 与 当前字符最后出现的位置进行比较
            // 若后者较大则更新当前片段右边界的位置
            end = max(end, char_index[s[i]]);
            // 当前的字符的位置 等于 当前片段右边界的位置
            if(i == end) {
                // 将当前片段的长度放入结果数组中
                result.push_back(end - start + 1);
                // 更新当前片段的左边界位置
                start = end + 1;
                // 更新当前片段的右边界位置
                end = char_index[s[start]];
            }
        }
        return result;
    }
};