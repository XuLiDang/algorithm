class Solution {
public:
    static bool cmp(const vector<int> &a, const vector<int> &b) {
        if(a[0] < b[0])
            return true;
        else
            return false;
    }

    // 合并区间，无重叠区间 以及 用最少数量的箭引爆气球 的解题思路大体一致
    // 这题的贪心思想: 尽可能多地合并互相重叠区间，使其形成一个大的未重叠区间
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        // 将数组中的元素按照左边界从小到大进行排序，使得左边界相邻的区间挨在一起
        sort(intervals.begin(), intervals.end(), cmp);
        // 结果数组
        vector<vector<int>> result;

        // left 和 right 分别用于记录当前不重叠区间的左右边界
        int left = intervals[0][0], right = intervals[0][1];

        for(int i = 1; i < intervals.size(); i++) {
            // 当前区间的左边界 小于或等于 不重叠区间的右边界
            if(intervals[i][0] <= right) {
                // 更新不重叠区间的左右边界
                left = min(left, intervals[i][0]);
                right = max(right, intervals[i][1]);
            }
            // 当前区间的左边界 大于 不重叠区间的右边界
            else {
                // 先将当前不重叠区间放入结果数组中
                result.push_back(vector<int> {left, right});
                // 更新不重叠区间的左右边界
                left = intervals[i][0];
                right = intervals[i][1];
            }
        }

        // 将最后的不重叠区间放入结果数组中
        result.push_back(vector<int> {left, right});
        return result;
    }
};