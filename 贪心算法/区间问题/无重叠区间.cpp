class Solution {
public:
    //  重载 < 运算符 
    static bool cmp(const vector<int>& a, const vector<int>& b) {
        if(a[0] < b[0])
            return true;
        else
            return false;
    }

    // 问题可以转换为: 保留尽可能多的互不重叠区间，使得未重叠区间里面的区间数尽可能地大，返回需要移除区间的数量
    // 因此采用贪心算法的思想的话: 当遇到区间之间发生重叠时，选择右边界较小的一方作为未重叠区间的右边界，从而将
    // 后面可能多的区间放入未重叠区间中。
    int eraseOverlapIntervals(vector<vector<int>>& intervals) {
        // 数组的大小为 0 或 1 时都不需要移除任何区间
        if(intervals.size() == 0 || intervals.size() == 1) 
            return 0;
        // 将数组中的元素按照左边界从小到大进行排序，使得左边界相邻的区间挨在一起
        sort(intervals.begin(), intervals.end(), cmp);
        // 未重叠区间的右边界
        int end = intervals[0][1];
        // 需要移除的区间数量
        int count = 0;

        for(int i = 1; i < intervals.size(); i++) {
            // 当前区间的左边界 大于等于 未重叠区间的右边界
            if(intervals[i][0] >= end)
                // 更新未重叠区间的右边界，相当于将当前区间也加入到未重叠区间中
                end = intervals[i][1];
            // 当前区间的左边界 小于 未重叠区间的右边界 
            else {
                // 我们需要比较 未重叠区间 与 当前区间 的右边界，并选择值较小的一方作为新的未重叠区间
                // 的右边界，这是为了将后面尽可能多的区间放入未重叠区间中。
                end = min(end, intervals[i][1]);
                // 移除一个区间，这个区间有可能是当前区间，也有可能是未重叠区间里面的区间
                count++;
            }
        }
        return count;
    }
};