/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int result = 0;

    int traversal(TreeNode* root) {
        // 告知父节点当前节点为空节点
        if(root == NULL)
            return 3;

        // 获取左孩子传递的信息
        int left = traversal(root->left);
        // 获取右孩子传递的信息
        int right = traversal(root->right);

        // 左右孩子中有一个要求当前节点安装摄像头
        if(left == 1 || right == 1) {
            result++;
            // 告知父节点，当前节点已安装摄像头
            return 2;
        }

        // 左右孩子中有一个已安装了摄像头
        else if(left == 2 || right == 2)
            // 告知父节点自己已被摄像头覆盖
            return 3;
        
        // 左右孩子已被摄像头覆盖 或者 为空节点
        else 
            // 要求父节点安装摄像头
            return 1;

    }

   
    /**
     * 根据题目的要求，我们应该优先在叶子节点的父节点中放置摄像头，随后每隔一个节点再放置一个摄像头。 
     * 很显然，每个节点都需要从其孩子节点中获得信息之后才能决定是否要在当前节点放置摄像头以及是否要
     * 求父节点放置摄像头等 (换句话说，就是向父节点传递信息)。二叉树有3种遍历，那就有3种信息交流的方
     * 式。前序的自上而下，后序的自下而上，中序的左上右下。而根据题意要求，我们应该选择后序遍历来传递
     * 信息。那么要传递的信息分为三种: 1.要求父节点安装摄像头; 2.说自己有摄像头; 3.自己已被摄像头覆  
     * 盖或当前节点为空节点;
    */
    // 本题的贪心算法思想在于: 每个摄像头覆盖到尽可能多的节点，因此优先在叶子节点的父节点中放置摄像头
    // 随后每隔一个节点再放置一个摄像头
    int minCameraCover(TreeNode* root) {
        // 根节点没有被摄像头覆盖，此时要在根节点上安装一个摄像头
        if(traversal(root) == 1)
            result++;

        return result; 
    }
};