class Solution {
public:
    int wiggleMaxLength(vector<int>& nums) {
        if(nums.size() < 2) 
            return nums.size();

        // 用于代表前一个趋势的状态，-1 代表下降，1 代表上升
        // 一开始并没有趋势，因此为 0 
        int trend = 0;
        // 用于记录最大的摆动次数
        int result = 0;

        // 1. 当前元素大于上一个元素且前一个趋势的状态不是上升
        // 2. 当前元素小于上一个元素且前一个趋势的状态不是下降
        // 只有上面两种情况才能增加摆动次数，并且遇到不符合条件
        // 的元素时直接跳过即可，这样看起来就像是删除了一些元素
        // 来获得最大子序列。
        for(int i = 1; i < nums.size(); i++) {
            // 当前元素大于上一个元素且前一个趋势的状态不是上升
            if(nums[i] > nums[i - 1] && trend != 1) {
                result++;
                trend = 1;
            }
            // 当前元素小于上一个元素且前一个趋势的状态不是下降
            if(nums[i] < nums[i - 1] && trend != -1) {
                result++;
                trend = -1;
            }
        }

        // 最大的摆动次数加1代表的便是最大摆动序列中的元素个数
        result++;
        return result;
    }   
};