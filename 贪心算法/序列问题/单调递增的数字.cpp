class Solution {
public:
    // 暴力算法思路: 从 n 开始，依次从大到小地判断当前数字是否为单调递增的整数
    // 若是则返回当前数字，否则继续往下判断。
    /**
     * 思路：
     *  从右向左扫描数字，若发现当前数字比其左边一位（较高位）小，
     *  则把其左边一位数字减1，并将该位及其右边的所有位改成9
     */
    int monotoneIncreasingDigits(int n) {
        if(n < 10)
            return n;

        string str = to_string(n);
        // index 用于记录 9 从哪里开始
        // 设置为这个默认值，为了防止第二个for循环在flag没有被赋值的情况下执行
        int index = str.size();

        for(int i = str.size() - 1; i > 0; i--) {
            if(str[i - 1] > str[i]) {
                index = i;
                str[i - 1]--;
            }
            else continue;
        }

        // index 和之后的位置都赋值为9
        for(int i = index; i < str.size(); i++) {
            str[i] = '9';
        }

        return stoi(str);
    }
};