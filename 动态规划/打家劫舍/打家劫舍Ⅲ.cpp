/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int rob(TreeNode* root) {
        // 用一个长度为 2 的数组来表示 dp 数组，dp[0] 表示不偷当前节点所得到的最大金钱
        // 而 dp[1] 表示的则是偷当前节点所得到的最大金钱。最后返回的是偷与不偷根节点之间
        // 的较大值，因此需要采用后序遍历从树的底部开始处理，并一层层地往上传递信息。
        vector<int> result = robtree(root);
        return max(result[0], result[1]);
    }

    vector<int> robtree(TreeNode* root) {
        // 当前节点为空节点，因此偷与不偷当前节点的值都是 0
        if(root == NULL) 
            return {0, 0};
        // 获取存放 偷与不偷左孩子所得到的最大金钱 的 dp 数组
        vector<int> left = robtree(root->left);
        // 获取存放 偷与不偷右孩子所得到的最大金钱 的 dp 数组
        vector<int> right = robtree(root->right);
        // 不偷当前节点，左右孩子可以偷也可以不偷，但一定是选择值最大的情况
        int val1 = max(left[0], left[1]) + max(right[0], right[1]);
        // 偷当前节点，那么左右孩子就不能偷
        int val2 = root->val + left[0] + right[0];
        // 返回偷与不偷当前节点所得到的最大金钱
        return {val1, val2};
    }
};