class Solution {
public:
    // 本题需要处理环的问题。因此可以分为两种情况来进行处理，第一种情况是从不包含尾元素的数组中得出
    // 一晚上所能偷窃到的最大金额。而另一种情况则是从不包含首元素的数组中得出一晚上所能偷窃到的最大
    // 金额。随后将这两个金额进行对比，并将较大值返回。
    int rob(vector<int>& nums) {
        // 数组为空 或 只有一个元素 
        if(nums.size() == 0) return 0;
        if(nums.size() == 1) return nums[0];
        // 不包含尾元素
        int result1 = robRange(nums, 0, nums.size() - 2);
        // 不包含首元素
        int result2 = robRange(nums, 1, nums.size() - 1);
        // 返回较大值
        return max(result1, result2);

    }

    int robRange(vector<int>& nums, int start, int end) {
        // 子区间只有一个元素
        if(start == end) return nums[start];
        // dp[i] 表示一晚上从以下标 start 开始的前 i - start + 1 间房屋中偷窃到的最大金额(start <= i <= end)
        vector<int> dp(nums.size());
        // 初始化
        dp[start] = nums[start];
        dp[start + 1] = max(nums[start], nums[start + 1]);

        // 下面的逻辑与 198.打家劫舍 一致
        for(int i = start + 2; i <= end; i++) {
            dp[i] = max(dp[i - 1], dp[i - 2] + nums[i]);
        }

        return dp[end];
    }
};