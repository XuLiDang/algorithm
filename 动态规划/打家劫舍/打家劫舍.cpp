class Solution {
public:
    int rob(vector<int>& nums) {
        // 数组为空 或 只有一个元素 
        if(nums.size() == 0) return 0;
        if(nums.size() == 1) return nums[0];

        // dp[i] 表示一晚上从下标前 i 间房屋中偷窃到的最大金额
        vector<int> dp(nums.size(), 0);
        // 初始化 dp 数组, dp[0] 没有意义
        dp[0] = nums[0];
        dp[1] = max(nums[0], nums[1]);

        // 1. 偷下标第 i 间房屋，那么 dp[i - 1] 的情况一定不考虑的。因为如果下标第 i - 1 间房屋被偷了，那么不考虑
        // dp[i - 1] 是正常的，而如果第 i - 1 间房屋没有被偷，那么 dp[i - 1] = dp[i - 2]，此时不考虑 dp[i - 1]
        // 也是正确的。 2. 不偷第 i 间房屋，那么很明显 dp[i] = dp[i - 1]
        for(int i = 2; i < nums.size(); i++) {
            dp[i] = max(dp[i - 1], dp[i - 2] + nums[i]);
        }

        return dp[nums.size() - 1];
    }

};