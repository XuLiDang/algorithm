class Solution {
public:
    int numDistinct(string s, string t) {
        int m = s.size();
        int n = t.size();
        // dp[i][j] 表示以 s 第 i 个元素结尾的序列中有多少个以 t 第 j 个元素结尾的序列
        // uint64_t 是为了计算 dp[i][j] 时出现值(int 型变量无法存放)溢出的情况
        vector<vector<uint64_t>> dp(m + 1, vector<uint64_t>(n + 1, 0));
       
        // t 为空串
        for(int i = 0; i <= m; i++) dp[i][0] = 1;
        // s 为空串
        for(int j = 1; j <= n; j++) dp[0][j] = 0;

        // 遍历字符串 s 中的元素
        for(int i = 1; i <= m; i++) {
            // 遍历字符串 t 中的元素
            for(int j = 1; j <= n; j++) {
                // 当 s[i - 1] = t[j - 1] 时，需要考虑两种情况，一种是使用 s[i - 1] 进行匹配，另一种则是不使用 s[i - 1]。
                // 来匹配.对于第一种情况，s[i - 1] 与 t[j - 1]正好抵消掉了，因此 dp[i][j] = dp[i - 1][j - 1]。而对于第二
                // 种情况来说 dp[i][j] = dp[i - 1][j]。注意，s 的第 i 个元素的下标为 i - 1，t 同理
                if(s[i - 1] == t[j - 1]) 
                    dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j];
                // s[i - 1] 与 t[j - 1] 不相等，说明肯定不能用 s[i - 1] 来匹配以 t[j - 1] 结尾的序列
                // 即模拟从 s 中删除 s[i - 1] 元素
                else
                    dp[i][j] = dp[i - 1][j];
            }
        }

        return dp[m][n];
    }
};