class Solution {
public:
    // 另一个解题思路: 求出两个序列的最长公共序列的长度 public_size，而最后的最少操作步数:
    // word1.size() + word2.size() - public_size * 2
    int minDistance(string word1, string word2) {
        int m = word1.size();
        int n = word2.size();
        // dp[i][j] 表示使得以 word1 第 i 个元素结尾的子串与以 word2 第 j 个元素结尾的子串相同所需的最小步数
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        // word1 为空串
        for(int i = 1; i <= n; i++)
           dp[0][i] = i;
        // word2 为空串
        for(int j = 1; j <= m; j++)
            dp[j][0] = j;

        for(int i = 1; i <= word1.size(); i++) {
            for(int j = 1; j <= word2.size(); j++) {
                // word1 的第 i 个元素等于 word2 的第 j 个元素，dp[i][j] = dp[i - 1][j - 1]
                // 注意，word1 的第 i 个元素的下标为 i - 1，word2 同理
                if(word1[i - 1] == word2[j - 1])
                    dp[i][j] = dp[i - 1][j - 1];
                // 1.删除 word1[i - 1] 与 word2[j - 1];
                // 2.删除 word2[j - 1];
                // 3.删除 word1[i- 1];
                else
                    dp[i][j] = min(dp[i - 1][j - 1] + 2, min(dp[i][j - 1], dp[i - 1][j]) + 1);
            }
        }
    
        return dp[m][n];
    }

};