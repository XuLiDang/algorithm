class Solution {
public:
    bool isSubsequence(string s, string t) {
        if(s.size() == 0) 
            return true;
        if(t.size() == 0)
            return false;
        // dp[i][j] 表示以 s 第 i 个元素结尾的子串 与以 t 第 j 个元素结尾的子串之间的最长公共子序列
        // 注意不是最长连续公共子序列
        vector<vector<int>> dp(s.size() + 1, vector<int>(t.size() + 1, 0));
       
        // s 为空串
        for(int i = 0; i <= t.size(); i++) dp[0][i] = 0;
        // t 为空串
        for(int j = 0; j <= s.size(); j++) dp[j][0] = 0;

        // 遍历 s 的元素
        for(int i = 1; i <= s.size(); i++) {
            // 遍历 t 的元素
            for(int j = 1; j <= t.size(); j++) {
                // s 的第 i 个元素等于 t 的第 j 个元素，dp[i][j] = dp[i - 1][j - 1] + 1
                // 注意，s 的第 i 个元素的下标为 i - 1，t 同理
                if(s[i - 1] == t[j - 1])
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                // s[i] != t[j]，此时可以继续比较 dp[i][j - 1] 与 dp[i - 1][j] 哪个值最大
                // dp[i][j - 1] 表示模拟删除 word2[j]，而 dp[i - 1][j] 模拟删除 word1[i]
                else
                    dp[i][j] = max(dp[i][j - 1], dp[i - 1][j]);
            }
        }

        // 字符串 s 和 t 之间的最长公共子序列的值等于 s 的长度就意味着 t 肯定可以包含 s
        if(dp[s.size()][t.size()] == s.size())
            return true;
        else
            return false;
    }
};