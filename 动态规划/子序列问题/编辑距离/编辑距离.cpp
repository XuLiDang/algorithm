class Solution {
public:
    int minDistance(string word1, string word2) {    
        // dp[i][j] 表示为 word1 的前 i 个字符转换成 word2 的前 j 个字符所使用的最少操作数。
        vector<vector<int>> dp(word1.size() + 1, vector<int>(word2.size() + 1, 0));
        // word1 为空串
        for(int i = 1; i <= word2.size(); i++) 
            dp[0][i] = i; 
        // word2 为空串
        for(int j = 1; j <= word1.size(); j++)
            dp[j][0] = j;

        for(int i = 1; i <= word1.size(); i++) {
            for(int j = 1; j <= word2.size(); j++) {
                //  word1[i - 1] == word2[j - 1] 的情况下，dp[i][j] = dp[i − 1][j − 1]
                // 注意，word1 的第 i 个元素的下标为 i - 1，word2 同理
                if(word1[i - 1] == word2[j - 1])
                    dp[i][j] = dp[i - 1][j - 1];
                // 1.将 word1[i - 1] 替换成 word2[j - 1]: dp[i - 1][j - 1] + 1
                // 2.在 word1[i - 1] 的后面添加一个 word2[j - 1]: dp[i][j - 1] + 1
                // 3.删除 word1[i - 1]: dp[i - 1][j] + 1
                else
                    dp[i][j] = min(dp[i - 1][j - 1], min(dp[i][j - 1], dp[i - 1][j])) + 1;
            }
        }

        return dp[word1.size()][word2.size()];
    }
};
