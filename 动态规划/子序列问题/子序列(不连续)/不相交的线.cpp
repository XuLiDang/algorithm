class Solution {
public:
    // 本题要求连接两个值相等的数字 nums1[i] 与 nums[j] 的直线不能相交，并且求出符合要求的最大的直线数量
    // 实际上就是求两个数组之间的最长公共子序列。例如，nmus1 = {1,4,2}，nmus2 = {1,2,4}，那么这两个数组
    // 之间的最长公共子序列就是 {1,4} 或者 {1,2}。而最大不相交的直线数量就等于最长公共子序列的长度。
    int maxUncrossedLines(vector<int>& nums1, vector<int>& nums2) {
        // dp[i][j] 表示以 nums1[i] 结尾的子串 与 以 nums2[j] 结尾的子串之间的最长公共子序列
        // 注意不是最长连续公共子序列
        vector<vector<int>> dp(nums1.size(), vector<int>(nums2.size(), 0));

        // 初始化第一个元素
        if(nums1[0] == nums2[0])
            dp[0][0] = 1;

        // 从下标 1 开始初始化第一行
        for(int i = 1; i < nums2.size(); i++) {
            if(nums1[0] == nums2[i]) 
                dp[0][i] = 1;
            else
                dp[0][i] = max(dp[0][i], dp[0][i - 1]);
        }
        // 从下标 1 开始初始化第一列
        for(int j = 1; j < nums1.size(); j++) {
            if(nums1[j] == nums2[0]) 
                dp[j][0] = 1;
            else
                dp[j][0] = max(dp[j][0], dp[j - 1][0]);
        }

        // 遍历 nums1 的元素
        for(int i = 1; i < nums1.size(); i++) {
            // 遍历 nums2 的元素
            for(int j = 1; j < nums2.size(); j++) {
                if(nums1[i] == nums2[j])
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                else 
                    dp[i][j] = max(dp[i][j - 1], dp[i - 1][j]);
            }
        }

        // 因为此题没有子序列递增等要求，因此最后的元素的值必定是最大的
        return dp[nums1.size() - 1][nums2.size() - 1];
    }
};