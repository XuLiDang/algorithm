class Solution {
public:
    int longestCommonSubsequence(string text1, string text2) {
        // dp[i][j] 表示以 text1 第 i 个元素结尾的子串 与以 text2 第 j 个元素结尾的子串之间的最长公共子序列
        // 注意不是最长连续公共子序列
        vector<vector<int>> dp(text1.size() + 1, vector<int>(text2.size() + 1, 0));

        // text1 为空串
        for(int i = 0; i <= text2.size(); i++) dp[0][i] = 0;
        // text2 为空串
        for(int j = 0; j <= text1.size(); j++) dp[j][0] = 0;

        // 遍历 text1 的元素
        for(int i = 1; i <= text1.size(); i++) {
            // 遍历 text2 的元素
            for(int j = 1; j <= text2.size(); j++) {
                if(text1[i - 1] == text2[j - 1])
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                else 
                    dp[i][j] = max(dp[i][j - 1], dp[i - 1][j]);
            }
        }

        // 因为此题没有子序列递增等要求，因此最后的元素的值必定是最大的
        return dp[text1.size()][text2.size()];
    }
};