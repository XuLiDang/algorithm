class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        if(nums.size() == 1)
            return 1;
        // dp[i] 表示以 nums[i] 元素结尾的最大递增子序列的长度，以任何元素结
        // 尾的最大递增子序列的长度至少为 1，因此将 dp 数组的元素均初始化为 1
        vector<int> dp(nums.size(), 1);
        // 最后返回的数组中最大递增子序列的长度
        int result = 0;

        // 从下标为 1 的元素开始，遍历整个数组
        for(int i = 1; i < nums.size(); i++) {
            // 遍历下标小于 i 的元素
            for(int j = 0; j < i; j++) {
                // 如果nums[j] 小于 nums[i]，则说明 nums[i] 可以接在 nums[j] 后面形成新的递增子序列
                // 此时需要更新 dp[i] 的值。 具体来说，dp[j] + 1 表示在以 nums[j] 元素结尾的最大递增
                // 子序列的基础上接上 nums[i]，从而形成更大的递增子序列。
                if(nums[i] > nums[j]) dp[i] = max(dp[i], dp[j] + 1);
            }
            // 更新当前最大递增子序列的长度
            result = max(result, dp[i]);
        }

        return dp[nums.size() - 1];
    }
};