class Solution {
public:
    int countSubstrings(string s) {
        int n = s.size();
        // dp[i][j] 表示下标范围[i,j]的字符串是否回文串，初始化均为 false
        vector<vector<bool>> dp(n, vector<bool>(n, false));
        // 用于记录字符串 s 的回文子串数量
        int result = 0;
        // dp[i][j] 代表的字符串是否回文依赖于 dp[i + 1][j - 1]。因此要从下到上，从左到右遍历即 i 要
        // 从大到小，j 要从小到大开始遍历，同时还要注意 dp[i][j] 的定义，因此 j 一定是大于等于 i 的
        for(int i = s.size() - 1; i >= 0; i--) {
            for(int j = i; j < s.size(); j++) {
                // s[i] 与 s[j] 相等，两者不相等则下标范围[i,j]的字符串肯定不是回文串
                if(s[i] == s[j]) {
                    // 下标 j 与 i 相差小于等于2，那么下标范围[i,j]的字符串肯定是回文串
                    if(j - i <= 2) {
                        result++;
                        dp[i][j] = true;
                    }
                    // 下标相差大于 2 则需要依靠 dp[i + 1][j - 1] 才能是否为回文串 
                    else {
                        // dp[i + 1][j - 1] 为 true 则 dp[i][j] 为 true，反之亦然
                        dp[i][j] = dp[i + 1][j - 1];
                        // dp[i][j]  为 true 则表示下标范围[i,j]的字符串是回文串
                        if(dp[i][j]) 
                            result ++;                       
                    }
                }
            }
        }

        return result;
    }
};