class Solution {
public:
    int longestPalindromeSubseq(string s) {
        // dp[i][j] 表示下标[i,j]范围的字符串中最长回文子序列(注意不是回文串)的长度
        vector<vector<int>> dp(s.size(), vector<int>(s.size(), 0));
        // 长度为 1 的字串的最长回文子序列的长度均为 1
        for(int i = 0; i < s.size(); i++) {
            dp[i][i] = 1;
        }

        // dp[i][j] 依赖于 dp[i + 1][j - 1]。因此要从下到上，从左到右遍历即 i 要从大到小
        // j 要从小到大开始遍历，同时还要注意 dp[i][j] 的定义，j 一定是大于等于 i 的，但是
        // i 与 j 相等的情况在前面已进行了处理，因此 j 就从 i + 1 开始
        for(int i = s.size() - 1; i >= 0; i--) {
            for(int j = i + 1; j < s.size(); j++) {
                // s[i] == s[j]，那么下标[i,j]范围的最长回文子序列的长度就等于 dp[i + 1][j - 1] + 2
                if(s[i] == s[j])
                    dp[i][j] = dp[i + 1][j - 1] + 2;
                // s[i] != s[j]，则说明s[i] 与 s[j] 无法同时加入下标 [i,j] 范围的最长回文子序列中
                // 因此分别加入 s[i] 与 s[j] 看看哪一个可以组成下标范围内最长的回文子序列。
                else
                    dp[i][j] = max(dp[i + 1][j], dp[i][j - 1]);
            }
        }

        return dp[0][s.size() - 1];
    }
};