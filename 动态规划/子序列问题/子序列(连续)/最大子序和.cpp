class Solution {
public:
    // 利用贪心算法求解，每次都追求局部最优，从而最后得到整体最优
    // 局部最优: 当前连续和小于0时立刻放弃，并从下一个元素开始计算 "连续和"。
    int maxSubArray(vector<int>& nums) {
        // 用于存放最大连续和
        int result = INT_MIN;
        // 当前连续和
        int count = 0;

        for(int i = 0; i < nums.size(); i++) {
            // 更新当前连续和
            count += nums[i]; 
            // 更新最大连续和
            if(count > result) 
                result = count;
            // 当前连续和小于0时立刻放弃，并从下一个元素开始重新计算 "连续和"。
            if(count < 0) 
                count = 0; 
        }

        return result;
    }

    // 动态规划法 
    int maxSubArray(vector<int>& nums) {
        // 数组的长度为1
        if(nums.size() == 1)
            return nums[0];

        // dp[i] 表示以 nums[i] 结尾(必须包含nums[i])的最大连续子序列和
        vector<int> dp(nums.size(), 0);
        // 初始化 dp 数组
        dp[0] = nums[0];
        // 用于记录最大连续子序列和
        int result = dp[0];

        // 遍历数组的元素
        for(int i = 1; i < nums.size(); i++) {
            // dp[i - 1] + nums[i] 即将 nums[i] 加入当前连续子序和
            // 而 nums[i] 则是从头开始计算当前连续子序和
            dp[i] = max(dp[i - 1] + nums[i], nums[i]);
            // 尝试更新最大连续子序列和
            result = max(result, dp[i]);
        }
        // 返回结果
        return result;
    }
};