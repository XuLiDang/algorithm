class Solution {
public:
    int findLength(vector<int>& nums1, vector<int>& nums2) {
        // dp[i][j] 表示以 nums1 第 i 个元素结尾的序列与以 nums2 第 j 个元素结尾的序列之间的最长公共子数组的长度
        // 即最长公共连续序列的长度
        vector<vector<int>> dp(nums1.size() + 1, vector<int>(nums2.size() + 1, 0));
        // 最终的结果
        int result = 0;
        // nums1 为空
        for(int i = 0; i <= nums2.size(); i++) dp[0][i] = 0;
        // nums2 为空
        for(int j = 0; j <= nums1.size(); j++) dp[j][0] = 0;

        // 遍历 nums1 的元素
        for(int i = 1; i <= nums1.size(); i++) {
            // 遍历 nums2 的元素
            for(int j = 1; j <= nums2.size(); j++) {
                if(nums1[i - 1] == nums2[j - 1]) 
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                // 更新最长连续序列的长度
                result = max(result, dp[i][j]);         
            }
        }

        return result;
    }
};