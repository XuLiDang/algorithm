class Solution {
public:
    int findLengthOfLCIS(vector<int>& nums) {
        if(nums.size() == 1)
            return 1;
        // dp[i] 表示以下标 i 结尾的最大连续递增子序列的长度，注意是连续
        vector<int> dp(nums.size(), 1);
        // 存放整个数组中最大连续递增子序列的长度
        int result = 0;
        // 遍历数组中的每个元素
        for(int i = 1; i < nums.size(); i++) {
            // 若当前元素值大于前一个元素值，则说明可以将nums[i] 接在 nums[i - 1] 的后面
            // 从而形成新的连续递增子序列。
            if(nums[i] > nums[i - 1]) 
                // dp[i - 1] + 1 表示在以 nums[i - 1] 结尾的最大连续递增子序列的后面接上 
                // nums[i]，从而形成更大的连续递增子序列。
                dp[i] = dp[i - 1] + 1;
            // nums[i] 小于 nums[i - 1]，则说明以 nums[i] 结尾的最大连续递增子序列的长度为1
            else
                dp[i] = 1;
            // 更新当前最大连续递增子序列的长度
            result = max(result, dp[i]);
        }

        return result;   
    }
};