class Solution {
public:
    // 回溯法求解，但时间复杂度为 O(2^n)，空间复杂度为 O(n)，即递归系统调用栈的空间。使用此方法会超时
    // 思路: 遍历分割字符串，然后判断子串是否存在字典中，若存在则继续判断后面的子串是否能在字典中找到
    // 当分割点 大于等于 字符串的长度时，代表可以利用字典中出现的单词拼接出整个字符串
    bool backtracking(string& s, unordered_set<string>& wordSet, int startIndex) {    
        // 当分割点 大于等于 字符串的长度时，代表整个字符串都能够被正确地分割 
        if(startIndex >= s.size())
            return true;

        // 切割子串
        for(int i = startIndex; i < s.size(); i++) {
            string word = s.substr(startIndex, i - startIndex + 1);
            // 当子串不存在字典中，则直接跳过该子串
            if(wordSet.find(word) == wordSet.end()) continue;
            // 当前子串存在字典中，且字符串中后序的子串也能在字典中找到
            // 此时则能说明以利用字典中出现的单词拼接出整个字符串
            if(backtracking(s, wordSet, i + 1) == true)
                return true;
        }

        return false;
    }

    bool wordBreak(string s, vector<string>& wordDict) {
        // 记录字典中存在的单词
        unordered_set<string> wordSet(wordDict.begin(), wordDict.end());
        return backtracking(s, wordSet, 0);
    }

    // 动态规划法，没必要硬套完全背包，当作为单纯的动态规划问题反倒更好理解
    bool wordBreak(string s, vector<string>& wordDict) {
        // dp[i] 表示可以利用字典中出现的单词拼接出字符串 s 的前 i 个字符
        int n = s.size();
        // 记录字典中存在的单词
        unordered_set<string> wordSet(wordDict.begin(), wordDict.end());
        vector<bool> dp(n + 1, false);
        dp[0] = true;

        // 遍历字符串，i 代表的不是下标，而是第几个元素
        for(int i = 1; i <= n; i++) {
            // 根据前面的状态 dp[j](j < i) 以及下标为 [j,i] 区间的子串是否出现在字典里来判断 dp[i] 是否为 true
            for(int j = 0; j < i; j++) {
                // 获取下标为 [j,i] 区间的子串
                string word = s.substr(j, i - j);
                // 表示可以利用字典中出现的单词拼接出字符串 s 的前 i 个字符则直接跳出内循环
                if(wordSet.find(word) != wordSet.end() && dp[j]) {
                    dp[i] = true;
                    break;
                } 
                else 
                    continue;
                
            }
        }

        // 返回结果
        return dp[n];
    }

};