class Solution {
public:
    // 该题也可以用回溯法
    int coinChange(vector<int>& coins, int amount) {
        // dp[j] 代表总金额为 j 的最少钱币个数
        vector<int> dp(amount + 1, INT_MAX);
        // 初始化 dp 数组
        dp[0] = 0;

        // 遍历物品
        for(int i = 0; i < coins.size(); i++) {
            // 遍历背包容量，本题没有涉及到组合和排列的问题，因此不需要在意物品和背包的遍历顺序
            for(int j = coins[i]; j <= amount; j++) {
                // dp[j - coins[i]] 的值为初始值则直接跳过
                if(dp[j - coins[i]] == INT_MAX)
                    continue;
                dp[j] = min(dp[j], dp[j - coins[i]] + 1);
            }
        }

        if(dp[amount] == INT_MAX)
            return -1;
        else
            return dp[amount];
    }   
};