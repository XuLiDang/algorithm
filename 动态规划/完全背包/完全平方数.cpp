class Solution {
public:
    int numSquares(int n) {
        // dp[j] 表示和为 j 的完全平方数的最少数量
        vector<int> dp(n + 1, INT_MAX);
        // 初始化 dp 数组
        dp[0] = 0;

        // 遍历物品，物品为完全平方数的根，完全平方数则是物品的重量，物品的值为 1。
        for(int i = 1; i * i <= n; i++) {
            // 遍历背包
            for(int j = i * i; j <= n; j++) {
                dp[j] = min(dp[j], dp[j - i * i] + 1);
            }
        }
        // 返回和为 n 的完全平方数的最少数量
        return dp[n];
    }
};