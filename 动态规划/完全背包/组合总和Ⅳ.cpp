class Solution {
public:
    // 这题也可以用回溯法完成
    int combinationSum4(vector<int>& nums, int target) {
        // dp[j] 表示总和为 j 的元素组合的个数(顺序不同的序列被视作不同的组合)
        vector<int> dp(target + 1, 0);
        // 初始化 dp 数组
        dp[0] = 1;

        // 先遍历背包
        for(int i = 0; i <= target; i++) {
            // 遍历物品，完全背包问题中求排列数的顺序是先遍历背包后遍历物品
            for(int j = 0; j < nums.size(); j ++) {
                // 背包的容量需 大于等于 当前物品的重量
                // dp[i] < INT_MAX - dp[i - nums[j]] 用来忽略相加和会超过 INT_MAX 的数
                if(i >= nums[j] && dp[i] < INT_MAX - dp[i - nums[j]])
                    dp[i] += dp[i - nums[j]];
            }
        }

        return dp[target];
    }
};