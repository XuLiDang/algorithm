class Solution {
public:
    int change(int amount, vector<int>& coins) {
        // dp[j] 代表总金额为 j 的组合数(不是排列)
        vector<int> dp(amount + 1, 0);
        // 初始化 dp 数组
        dp[0] = 1;

        // 先遍历物品
        for(int i = 0; i < coins.size(); i++) {
            // 遍历背包容量，注意这里每个硬币可以取无数次，因此是一个完全背包问题
            // 那么就要从小到大遍历背包容量
            for(int j = coins[i]; j <= amount; j++) {
                dp[j] += dp[j - coins[i]];
            }
        }

        return dp[amount];
    }
};