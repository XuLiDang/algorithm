class Solution {
public:
    // 从序列中选择子序列使得和接近 target 系列的题目
    // 本题其实就是尽量将石头分成重量相同的两堆，相撞之后剩下的石头最小
    int lastStoneWeightII(vector<int>& stones) {
        int sum = 0;
        // 求出石头堆的重量总和
        for(int i = 0; i < stones.size(); i++)
            sum += stones[i];
        // 石头堆重量总和的一半
        int target = sum / 2;

        // dp[j] 代表 总重量不超过 j 的石头堆的最大值，所谓的值也是石头的重量
        // dp[target] = target 则意味者能在石头堆中刚好找到部分石头的总量和为石头堆总和的一半，那么另一半的重量
        // 也一定等于石头堆总和的一半。因此相撞之后能完全抵消。但是 dp[target] 也有可能小于 target，那么另一半的
        // 重量就比当前这一半大，相撞之后剩下的就是 sum - dp[target] - dp[target]。      
        vector<int>dp ((sum / 2 + 1), 0);

        // 遍历石头
        for(int i = 0; i < stones.size(); i++) {
            // 遍历容量(重量)
            for(int j = target; j >= stones[i]; j--) {
                dp[j] = max(dp[j], dp[j - stones[i]] + stones[i]);
            }
        }

        return sum - dp[target] - dp[target];
    }       
};