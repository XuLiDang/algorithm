class Solution {
public:
    // 从序列中选择子序列使得和接近 target 系列的题目
    // 本题要求集合里能否出现总和为 sum / 2 的子集。
    // 采用01背包方法求解。元素的值既是元素的重量，同时也是元素的价值，而背包的容量则是数组元素总和的一半
    // 假设背包容量为 target，那么只要找到 dp[target] = target，就代表我们找到了总和为 target 的子集
    bool canPartition(vector<int>& nums) {
        int sum = 0;
        // 计算数组元素总和
        for(int i = 0; i < nums.size(); i++)
            sum += nums[i];
        // 元素总和为奇数则直接返回 false
        if(sum % 2 != 0) return false;
        // 背包容量
        int target = sum / 2;
        // 定义 dp 数组
        vector<int> dp((sum / 2 + 1), 0);

        for(int i = 0; i < nums.size(); i++) {
            // 每一个元素一定是不可重复放入，所以从大到小遍历
            for(int j = target; j >= nums[i]; j--) {
                dp[j] = max(dp[j], dp[j - nums[i]]+ nums[i]);
            }
        }

        if(dp[target] == target) return true;
        else return false;
    }
};