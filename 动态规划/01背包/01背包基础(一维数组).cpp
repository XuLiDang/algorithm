#include <iostream>
using namespace std;

void bag_problem2() {
	// 一共有3个物品
	vector<int> weight = {1, 3, 4};
	vector<int> value = {15, 20, 30};
	// 背包容量
	int bagWeight = 4;
	// 1.dp[j] 代表总重量不大于 j 的物品组合的最大价值
	vector<int> dp(bagWeight + 1, 0);

	// 2.递推公式: dp[j] = max{dp[j], dp[j - weight[i]] + value[i]}
	// 3.初始化 dp 数组，定义时已创建。

	// 遍历物品
	for(int i = 0; i < weight.size(); i++) {
		// 遍历容量
		for(j = bagWeight; j >= weight[i]; j--) {
			dp[j] = max{dp[j], dp[j - weight[i]] + value[i]};
		}
	}

	cout << dp[bagWeight] << endl;
}

int main() {
	bag_problem2();
}