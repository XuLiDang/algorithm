class Solution {
public:
    // 这题其实也可以用回溯算法来做，但是会超时
    int findMaxForm(vector<string>& strs, int m, int n) {
        // dp[i][j] 代表 最多有 i 个 0 和 j 个 1 的最大子集的大小为dp[i][j]
        // dp[0][0] 没有意义
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));

        // 递推公式: dp[i][j] = max{dp[i][j], dp[i - zeroNums][j - oneNums] + 1}
        // 这个每个物品的值就是1，而重量分为两个维度，即 0 有多少个 与 1 有多少个
        // 根据 dp[i][j] 的含义，只需将所有项都初始化为0即可

        // 遍历物品
        for(string str : strs) {
            // 计算当前字符串中 '0' 和 '1' 的个数
            int zeroNums = 0; int oneNums = 0;
            for(char c : str) {
                if(c == '0') 
                    zeroNums++;
                else
                    oneNums++;
            }

            // 遍历背包容量且从后向前遍历
            for(int i = m; i >= zeroNums; i--) {
                for(int j = n; j >= oneNums; j--) {
                    dp[i][j] = max(dp[i][j], dp[i - zeroNums][j - oneNums] + 1);
                }
            }
        }

        return dp[m][n];
    }
};