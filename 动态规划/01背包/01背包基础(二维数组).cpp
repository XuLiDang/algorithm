#include <iostream>
using namespace std;

void bag_problem1() {
	// 一共有3个物品
	vector<int> weight = {1, 3, 4};
	vector<int> value = {15, 20, 30};
	// 背包容量
	int bagWeight = 4;
	// 1. 定义 dp 二维数组，dp[i][j] 表示从下标为[0-i]的物品里任意取，选中的物品总重量不大于 j 的最大价值总和
	vector<vector<int>> dp(weight.size(), vector<int>(bagWeight + 1, 0));

	// 2. 递推公式: dp[i][j] = max{dp[i - 1][j], dp[i - 1][j - weight[i]] + value[i]}。当不将物品 i 放入
	// 背包时，dp[i][j] = dp[i - 1][j]。而要将物品 i 放入背包，那么首先就要获取从下标[0 - i-1]的物品里任意取，
	// 选中的物品总重量不大于 j - weight[i] 的最大价值总和，随后再加上物品 i 的值，代表将物品 i 放入背包后的最
	// 大价值总和。这里要求物品总重量不大于 j - weight[i] 是为防止加入物品 i 之后，物品的总重量超过 j。

	// 3. 初始化，当 j = 0 时，代表选中物品的总重量不能大于0，因此物品的总价值一定为0
	for(int i = 0; i < weight.size(); i++) {
		dp[i][0] = 0;
	}

	// 遍历物品
	for(int i = 0; i < weight.size(); i++) {
		// 遍历容量
		for(int j = 1; j <= bagWeight; j++) {
			// 物品 i 的重量大于当前要求的物品总重量，不能将物品 i 放入背包
			if(weight[i] > j) 
				dp[i][j] = dp[i - 1][j];
			else
				dp[i][j] = max{dp[i - 1][j], dp[i - 1][j - weight[i]] + value[i]}
		}
	}

	// 输出背包的最大价值
	cout << dp[weight.size() - 1][bagWeight] << endl;
}

int main() {
	bag_problem1();
}