class Solution {
public:
    // 每个数字都有两种状态：被进行 "+"， 或者被进行 "-"，因此可以将数组分成A和B两个部分: A部分的数字全部进行 "+" 操作
    // B部分的数字全部进行 "-" 操作。设数组的和为 sum，A部分的和为 sumA，B部分的和为 sumB。那么根据上面的分析，我们可以
    // 得出 sumA + sumB = sum (1); sumA - sumB = target (2); 将 1 与 2 相加，可以得到 2sumA = sum + target (3)
    // 即 sumA = (sum + target) / 2; 那么我们接下来的问题就是求出值为 sumA 的元素组合有多少种
    int findTargetSumWays(vector<int>& nums, int S) {
        int sum = 0;
        // 求出数组元素的总和
        for (int i = 0; i < nums.size(); i++) 
            sum += nums[i];
        // 此时没有方案
        if (abs(S) > sum) return 0; 
        // 此时没有方案
        if ((S + sum) % 2 == 1) return 0; 
        // 背包的容量
        int bagSize = (S + sum) / 2;
        // dp[j] 表示: 填满j（包括j）这么大容积的包，有dp[j]种方法
        vector<int> dp(bagSize + 1, 0);
        dp[0] = 1;
        for (int i = 0; i < nums.size(); i++) {
            for (int j = bagSize; j >= nums[i]; j--) {
                dp[j] += dp[j - nums[i]];
            }
        }
        return dp[bagSize];
    }
};

class Solution {
private:
    int result = 0;
    void backtracking(vector<int>& candidates, int target, int sum, int startIndex) {
        if (sum == target) {
            result++;
        }
        // 如果 sum + candidates[i] > target 就终止遍历
        for (int i = startIndex; i < candidates.size() && sum + candidates[i] <= target; i++) {
            sum += candidates[i];
            backtracking(candidates, target, sum, i + 1);
            sum -= candidates[i];
        }
    }
public:
    int findTargetSumWays(vector<int>& nums, int S) {
        int sum = 0;
        for (int i = 0; i < nums.size(); i++) sum += nums[i];
        if (S > sum) return 0; // 此时没有方案
        if ((S + sum) % 2) return 0; // 此时没有方案，两个int相加的时候要各位小心数值溢出的问题
        int bagSize = (S + sum) / 2; // 转变为组合总和问题，bagsize就是要求的和

        // 以下为回溯法代码
        sort(nums.begin(), nums.end()); // 需要排序
        backtracking(nums, bagSize, 0, 0);
        return result;
    }
};