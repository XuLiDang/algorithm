class Solution {
public:
    // 贪心算法思想: 同时维护买入股票的最小值和当前天数所能获得的最大利润，在遍历每个元素时，首先尝试更新买
    // 入股票的最小值，随后计算再当前天数所能获得的最大利润，并将它与当前最大利润进行对比来尝试更新最大利润
    int maxProfit(vector<int>& prices) {
        // 保存当前最小值
        int minValue = INT_MAX;
        // 最大利润
        int result = 0;

        for(int i = 0; i < prices.size(); i++) {
            // 尝试更新买入股票的最小值
            minValue  = min(prices[i], minValue);
            // 尝试更新最大利润
            result = max(result, prices[i] - minValue);
        }

        return result;
    }

    // 动态规划法，只能进行一次交易且不能当天买当天卖
    int maxProfit(vector<int>& prices) {
        // dp[i][0] 表示第 i 天持有股票的最多现金
        // dp[i][1] 表示第 i 天不持有股票的最多现金
        vector<vector<int>> dp(prices.size(), vector<int>(2, 0));
        // 第 0 天持有 或 不持有股票的最多现金
        dp[0][0] = - prices[0];
        dp[0][1] = 0;

        for(int i = 1; i < prices.size(); i++) {
            // 第 i - 1 天就持有股票，那么就保持现状，所得现金就是昨天持有股票的所得现金 即 dp[i - 1][0]
            // 第 i 天买入股票，股票只能买卖一次，所以当第 i 天买入股票的时，所持有的现金只能是 dp[i][0]- prices[i]
            dp[i][0] = max(dp[i - 1][0], dp[i][0] - prices[i]);
            // 第 i - 1 天就不持有股票，那么就保持现状，所得现金就是昨天不持有股票的所得现金 即 dp[i - 1][1]
            // 第 i 天卖出股票，所得现金就是按照今天股票价格卖出后所得现金即：prices[i] + dp[i - 1][0]
            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i]);
        }

        return dp[prices.size() - 1][1];
    }
};