class Solution {
public:
    int maxProfit(vector<int>& prices) {
        // 数组长度为 1 则直接返回 0
        if(prices.size() == 1) 
            return 0;

        // 一共有 4 种状态
        // 0. 持有股票的状态; 1. 卖出股票的状态(卖出了股票且度过了冷冻期)
        // 2. 今天卖出股票的状态; 3. 冷冻期的状态(即前一天刚好卖出了股票)
        vector<vector<int>> dp(prices.size(), vector<int>(4, 0));
        // dp[0][1]，dp[0][2]，dp[0][3] 均为 0
        dp[0][0] = -prices[0];

        for(int i = 1; i < prices.size(); i++) {
            // 前一天就是持有股票状态 或 前一天是冷冻期 或 前一天是卖出股票的状态
            dp[i][0] = max(dp[i - 1][0], max(dp[i - 1][1] - prices[i], dp[i - 1][3] - prices[i]));
            // 前一天就是卖出股票的状态 或 前一天是冷冻期
            dp[i][1] = max(dp[i - 1][1], dp[i - 1][3]);
            // 前一天只能为持有股票的状态
            dp[i][2] = dp[i - 1][0] + prices[i];
            // 前一天只能为今天卖出股票的状态
            dp[i][3] = dp[i - 1][2];
        }

        int n = prices.size() - 1;
        // 返回值，即最大利润为状态 1，2，3 之间的最大值
        return max(dp[n][1], max(dp[n][2], dp[n][3]));
    }
};