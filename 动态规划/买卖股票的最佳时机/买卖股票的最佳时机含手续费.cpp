class Solution {
public:
    // 这题跟 买卖股票的最佳时机Ⅱ 唯一的区别在于多了一个减去手续费的操作
    int maxProfit(vector<int>& prices, int fee) {
        if(prices.size() == 1)
            return 0;

        int n = prices.size();
        vector<vector<int>> dp(n, vector<int>(2, 0));
        dp[0][0] = - prices[0];
        dp[0][1] = 0;

        for(int i = 1; i < prices.size(); i++) {
            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i] - fee);
        }

        return dp[n - 1][1];
    }
};