class Solution {
public:
    // 贪心算法策略: 如果今天买的股票在明天卖出后能挣取利润，则买入今天的股票，并在明天卖出
    // 这样就能保证每次买卖都是挣钱的，因此可以得出最优解
    int maxProfit(vector<int>& prices) {
        int profit = 0;

        for(int i = 0; i < prices.size() - 1; i++) {
            if(prices[i + 1] > prices[i])
                profit += prices[i + 1] - prices[i];      
        }

        return profit;
    }

    // 动态规划法，可以进行无数次交易且可以当天买当天卖
    int maxProfit(vector<int>& prices) {
        // dp[i][0] 表示下标第 i 天持有股票所得的最大金额
        // dp[i][1] 表示下标第 i 天不持有股票所得的最大金额
        vector<vector<int>> dp(prices.size(), vector<int>(2, 0));
        // 初始化 dp 数组
        dp[0][0] = -prices[0];
        dp[0][1] = 0;

        for(int i = 1; i < prices.size(); i++) {
            // 在本题中，一只股票可以买卖多次，所以当第 i 天买入股票的时候，所持有的现金可能有之前买卖过的利润
            // 所以这里是 dp[i - 1][1] - prices[i]，而 121 题则是 dp[i - 1][0] - prices[i]
            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
            // 第 i - 1 天就不持有股票，那么就保持现状，所得现金就是昨天不持有股票的所得现金 即 dp[i - 1][1]
            // 第 i 天卖出股票，所得现金就是按照今天股票价格卖出后所得现金即：prices[i] + dp[i - 1][0]
            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i]);
        }

        return dp[prices.size() - 1][1];

    }
};