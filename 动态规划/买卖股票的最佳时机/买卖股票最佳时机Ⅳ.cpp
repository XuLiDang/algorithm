class Solution {
public:
    // 只能进行 k 次交易但可以当天买当天卖，k 为 2 时，该题就是买卖股票的最佳时机Ⅲ
    int maxProfit(int k, vector<int>& prices) {
        // 数组元素为 0
        if(prices.size() == 0)
            return 0;
        // 交易的次数为 k，则代表每一天都有 2 * k + 1 种状态，那么除了第 0 种状态之外
        // 奇数代表的就是第 i 次持有股票的状态，而偶数代表的则是第 i 次不持有股票的状态。
        vector<vector<int>> dp(prices.size(), vector<int>(2 * k + 1, 0));
        // 第 0 天不持有股票状态的值都为0，则持有股票状态的值都为 -1
        for(int i = 1; i < 2 * k; i += 2) {
            dp[0][i] = -prices[0];
        }

        // 从下标第 1 天开始遍历处理每一天
        for(int i = 1; i < prices.size(); i++) {
            // j + 2 < 2 * k + 1 -> j < 2 * k - 1
            // 遍历处理每一天的状态
            for(int j = 0; j < 2 * k - 1; j += 2) {
                // 奇数状态
                dp[i][j + 1] = max(dp[i - 1][j + 1], dp[i - 1][j] - prices[i]);
                // 偶数状态
                dp[i][j + 2] = max(dp[i - 1][j + 2], dp[i - 1][j + 1] + prices[i]);
            }
        }

        return dp[prices.size() - 1][2 * k];
    }
};