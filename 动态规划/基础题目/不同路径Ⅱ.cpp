class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        // 网格的行数
        int m = obstacleGrid.size();
        // 网格的列数
        int n = obstacleGrid[0].size();
        // 网格左上角或右下角有障碍物
        if(obstacleGrid[0][0] == 1 || obstacleGrid[m - 1][n - 1] == 1)
            return 0;
        
        // 1. dp[i][j] 代表到达 i 行 j 列有多少条路径
        vector<vector<int>> dp(m, vector<int>(n, 0));   
        // 2. 递推公式: dp[i][j] = dp[i - 1][j] + dp[i][j - 1]，每个位置只能通过正上方往下走或者左边往
        // 右走才能到达，因此去往每个位置的路径数就 等于 去往正上方位置的路径数 加上 去往左边位置的路径数
        // 3. 初始化 dp 数组，网格左上角的路径数为 0。
        // 第一列所有位置的路径数都为 1，遇到障碍物则通往该位置以及下面位置的路径数为 0
        for(int i = 0; i < m; i++) {
            // 当前位置上没有障碍物
            if(obstacleGrid[i][0] == 0)
                dp[i][0] = 1;
            // 当前位置上有障碍物，那么通往该位置以及下面位置的路径数为 0
            else
                break;
        }
        // 第一行的所有位置的路径数都为 1，遇到障碍物则通往该位置以及右边位置的路径数为 0
        for(int i = 0; i < n; i++) {
            // 当前位置上没有障碍物
            if(obstacleGrid[0][i] == 0)
                dp[0][i] = 1;
            // 当前位置上有障碍物，那么通往该位置以及右边位置的路径数为 0
            else
                break;
        }

        // 4. 遍历 dp 数组
        for(int i = 1; i < m; i++) {
            for(int j = 1; j < n; j++) {
                if(obstacleGrid[i][j] == 0)
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }

        return dp[m - 1][n - 1];
    }
};