class Solution {
public:
    int integerBreak(int n) {
        // 1. dp 数组用于存放整数拆分后的最大乘积，dp[i] 则代表拆分整数 i 的最大乘积
        vector<int> dp(n + 1);
        // 2. dp[i] 的值可以通过两个方法获得，一个是 j * (i - j) 直接相乘。一个是 j * dp[i - j]，同时 1 <= j < i - j 
        // 例如，我们要计算整数4拆分后的最大乘积，即计算dp[4]的值。 j = 1 时，j * (i - j) = 1 * 3 = 3，而 j * dp[i - j]
        // = 1 * dp[3]。 j = 2 时，j * (i - j) = 2 * 2 = 4，而 j * dp[i - j] = 2 * dp[2]。 j * (i - j) 可看作是单纯的
        // 把整数拆分为两个数相乘，而 j * dp[i - j] 则是拆分成了两个以及两个以上的个数相乘。因此递归公式: dp[i] = max{(i - j) 
        // * j, dp[i - j] * j}，因为在计算dp[i]的值时会有多种可能，因此我们还需要每次都跟当前 dp[i] 进行对比才能获得最大的dp[i]
        // 因此，最后的递归公式: dp[i] = max{dp[i], max{(i - j) * j, dp[i - j] * j}}

        // 3. 初始化 dp 数组，dp[0] 和 dp[1] 是没有意义的，dp[2] = 1 代表整数2拆分后的最大乘积为1
        // 即将整数2拆分成两个1。
        dp[2] = 1;
        // 4. dp[i] 是依靠 dp[i - j]的状态，所以遍历i一定是从前向后遍历，先有dp[i - j]再有dp[i]
        for (int i = 3; i <= n ; i++) {
            // dp[0] 和 dp[1] 是没有意义的，i - j 必须大于1，因此 j < i - 1
            for (int j = 1; j < i - 1; j++) {
                dp[i] = max(dp[i], max((i - j) * j, dp[i - j] * j));
            }
        }
        return dp[n];
    }
};