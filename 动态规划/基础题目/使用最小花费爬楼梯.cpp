class Solution {
public:
    int minCostClimbingStairs(vector<int>& cost) {
        // 1.dp数组的每个元素代表的是 到达第 i 个台阶的最低花费
        vector<int> dp(cost.size() + 1);

        // 2. 递推公式: dp[i] = min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
        // 3. 初始化 dp 数组，我们可以选择从下标为 0 或下标为 1 的台阶开始爬楼梯，所以dp[0] = dp[1] = 0
        dp[0] = 0;
        dp[1] = 0;
        // 4. 根据题意，我们需要从前往后遍历
        for(int i = 2; i < dp.size(); i++) {
            dp[i] = min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
        }
        return dp[cost.size()];
    }
};