class Solution {
public:
    int climbStairs(int n) {
        if(n == 1 || n == 2)
            return n;
        // 1.dp[i] 代表的是走 i 个台阶有多少种方法。 i >= 1
        vector<int> dp(n + 1);
        // 2. 第 n 个台阶只能从 n - 1 与 n - 2 个台阶上来。对于 n - 1 个台阶，此时需要往前跨两个台阶
        // 对于 n - 2 个台阶，此时则需要往前跨一个台阶即可。因此递推公式: dp[n] = dp[n - 1] + dp[n - 2]
        // 3.初始化 dp 数组，第一个和第二个台阶的走法分别为 1 和 2，dp[0] 没有意义，因此不使用
        dp[1] = 1;
        dp[2] = 2;

        // 4.根据递归公式，我们可以得知遍历顺序是从前到后的
        for(int i = 3; i <= n; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }

        return dp[n];
    }
};