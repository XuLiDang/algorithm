class Solution {
public:
    // 递归
    int traversal(int n) {
        if(n == 0 || n == 1)
            return n;

        int n1 = traversal(n - 1);
        int n2 = traversal(n - 2);

        return n1 + n2;
    }

    // 递归
    int fib(int n) {
        return traversal(n);
    }

    // 动规
    int fib(int n) {
        if(n <= 1) 
            return n;

        // 1.动规数组用于存放斐波那契数列的值，dp[i] 代表的则是第i个数的值
        vector<int> dp(n + 1);
        // 2.题目已给出递归公式: dp[i] = dp[i - 1] + dp[i + 2]
        // 3.初始化斐波那契数列
        dp[0] = 0;
        dp[1] = 1;

        // 4.根据递归公式，我们可以得知遍历顺序是从前到后的
        for(int i = 2; i < dp.size(); i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }

        return dp[n];
    }
};