class Solution {
public:
    int uniquePaths(int m, int n) {
        // 1. dp[i][j] 代表到达 i 行 j 列有多少条路径
        vector<vector<int>> dp(m, vector<int>(n, 0));
        // 2. 递推公式: dp[i][j] = dp[i - 1][j] + dp[i][j - 1]，每个位置只能通过正上方往下走或者左边往
        // 右走才能到达，因此去往每个位置的路径数就 等于 去往正上方位置的路径数 加上 去往左边位置的路径数

        // 3. 初始化 dp 数组，网格左上角的路径数为 0。而第一列所有位置的路径都为 1
        // 同时每列第一行的所有位置的路径也都为 1
        dp[0][0] = 0;
        for(int i = 0; i < m; i++) dp[i][0] = 1;
        for(int i = 0; i < n; i++) dp[0][i] = 1;

        // 4. 遍历 dp 数组
        for(int i = 1; i < m; i++) {
            for(int j = 1; j < n; j++) {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }

        return dp[m - 1][n - 1];
    }
};