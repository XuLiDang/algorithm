class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums) {
        // 滑动窗口起始位置
        int left = 0, right = 0;
        // 滑动窗口数值之和
        int sum = 0;
        // 最小长度
        int minLen = INT32_MAX;
        // 遍历数组
        while(right < nums.size()) {
            // 更新滑动窗口的数值
            sum += nums[right];
            // 滑动窗口的数值 大于或等于 target
            while(sum >= target) {
                // 获得当前滑动窗口的长度
                int len = right - left + 1;
                // 尝试更新最小长度
                minLen = minLen < len ? minLen : len;
                // 窗口左边往右移动，并更新滑动窗口的数值
                sum -= nums[left];
                left++;
            }
            // 滑动窗口右端往右移动
            right++;
        }
        // 如果 minLen 没有被赋值的话，就返回0，说明没有符合条件的子数组
        if(minLen == INT32_MAX)
            return 0;
        else
            return minLen;
    }
};