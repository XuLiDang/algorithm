#include <iostream>
#include <vector>
using namespace std;

// 题目可以理解为找出最多包含两种元素的连续最长子序列
// 利用快慢指针实现滑动窗口
int totalFruit(vector<int>& fruits) {
        // 滑动窗口起始位置 
        int left = 0, right = 0;
        // 最大连续子数组的长度
        int maxLen = 0;
        // 滑动窗口中元素的种类
        int type = 0;
        // 用于记录 fruits 数组中的元素在窗口中的个数
        vector<int> cnt(fruits.size(), 0);

        while(right < fruits.size()) {
            // fruits[right] 类型的元素不在当前窗口中
            if(cnt[fruits[right]] == 0) {
                // 增加窗口中所包含的元素类别
                type++;
                // 将fruits[right]类型的元素放入窗口
                cnt[fruits[right]]++;
                // 窗口内元素的种类大于2，此时需要从窗口左端开始丢掉前面所包含的元素
                while (type > 2) {
                    cnt[fruits[left]]--;
                    if(cnt[fruits[left]] == 0)
                        type--;
                    // 窗口左端向右划，缩小窗口范围
                    left++;
                }
            }
            // fruits[right] 类型的元素已在当前窗口中
            else {
                // 将fruits[right]类型的元素放入窗口
                cnt[fruits[right]]++;
            }

            int len = right - left + 1;
            maxLen = maxLen > len ? maxLen : len;
            // 窗口右端继续向右移动
            right++;
        }

        return maxLen;
    }

int main(int argc, char **argv)
{
    vector<int> nums = {0,1,2,2};
    printf("result is %d\n", totalFruit(nums));
    return 0;
}