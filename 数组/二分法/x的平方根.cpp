class Solution {
public:
    // 从题目的要求和示例我们可以看出，这其实是一个查找整数的问题，并且这个整数是有范围的。
    // 如果这个整数的平方 恰好等于 输入整数，那么我们就找到了这个整数；如果这个整数的平方
    // 严格大于 输入整数，那么这个整数肯定不是我们要找的那个数；如果这个整数的平方 严格小
    // 于 输入整数，那么这个整数 可能 是我们要找的那个数（重点理解这句话）。
    // 因此我们可以使用「二分查找」来查找这个整数，不断缩小范围去猜。猜的数平方以后大了就往小了猜；
    // 猜的数平方以后恰恰好等于输入的数就找到了；猜的数平方以后小了，可能猜的数就是，也可能不是。
    int mySqrt(int x) {
        // x 为 0 或 1，那么它的算法平方根就是它自己
        if(x == 0 || x == 1)
            return x;
        // left 和 right 就是我们要查找的整数的范围，result 则是返回值
        int left = 1, right = x, result = 0;
        
        while(left <= right) {
            // left + (right - left) / 2 等价于 (left + right) / 2
            int mid = left + (right - left) / 2;
            // 用 long 是为了防止溢出
            if((long)mid *mid < x) {
                // mid * mid < x，说明 mid 有可能是目标整数，因此需要用 result 记录
                result = mid;
                left = mid + 1;
            }
            // 用 long 是为了防止溢出
            else if ((long)mid *mid > x)
                right = mid - 1;
            // mid * mid = x
            else
                return mid;
        }
        return result;
    }
};

   