#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        // 数组为空 或 target 不在数组的元素值范围内
        if(nums.size() == 0 || target < nums[0] || target > nums[nums.size() - 1])
            return{-1, -1};
        // 获得左边界的下标
        int leftBorder = getLeftBorder(nums, target);
        // 获得右边界的下标
        int rightBorder = getRightBorder(nums, target);
        // 数组中找不到值为 target 的元素，其实这里可以省略，但为了完整还是加上这条语句
        if (leftBorder == -1 || rightBorder == -1) return {-1, -1};
        // 返回左右边界
        return {leftBorder, rightBorder};
    }
private:
     int getRightBorder(vector<int>& nums, int target) {
        int left = 0;
        int right = nums.size() - 1;
        // 记录 rightBorder 没有被赋值的情况
        int rightBorder = -1; 
        // 执行二分查找过程，唯一的区别在于 nums[middle] = target 时要更新右边界并且继续往右查找而不是直接返回
        while (left <= right) {
            int middle = left + ((right - left) / 2);
            if (nums[middle] > target) 
                right = middle - 1;
            else if(nums[middle] < target)
                left = middle + 1;
            // nums[middle] = target 则更新右边界，并且继续往右查找是否还有值等于 target 的元素
            else {
                rightBorder = middle; 
                left = middle + 1;     
            }
        }
        return rightBorder;
    }
    int getLeftBorder(vector<int>& nums, int target) {
        int left = 0;
        int right = nums.size() - 1;
        // 记录 leftBorder 没有被赋值的情况
        int leftBorder = -1; 

        // 执行二分查找过程，唯一的区别在于 nums[middle] = target 时要更新左边界并且继续往右查找而不是直接返回
        while (left <= right) {
            int middle = left + ((right - left) / 2);
            if (nums[middle] > target) 
                right = middle - 1;
            else if(nums[middle] < target)
                left = middle + 1;
            // nums[middle] = target 则更新左边界，并且继续往左查找是否还有值等于 target 的元素
            else {
                leftBorder = middle;
                right = middle - 1;
            }
        }
        return leftBorder;
    }
};