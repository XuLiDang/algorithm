class Solution {
public:
    // 该题与 x的平方根 一题的思路基本一致
    bool isPerfectSquare(int num) {
        // num = 1，那么它一定可写成1的平方
        if(num == 1)
            return true;
        // 从 left 到 right 的范围内查找平方为 num 的整数，或者查找 num 的算术平方根
        // x的平方根 一题的不同点在于，这里的算术平方根只能恰好为整数而不是近似为整数
        int left = 1, right = num;
        // 采用二分法来查找符合条件的整数
        while(left <= right) {
            int mid = left + (right - left) / 2;
            if((long)mid *mid < num)
                left = mid + 1;
            else if((long)mid *mid > num)
                right = mid - 1;
            else
                return true;
        }
        return false;
    }
};