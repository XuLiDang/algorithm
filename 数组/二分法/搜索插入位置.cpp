#include <iostream>
#include <vector>
using namespace std;

// 版本一，[left, right]
// 左闭右闭，因此当target小于nums[mid]时，mid = right - 1; 并且循环判定条件为left <= right
int searchInsert1(vector<int>& nums, int target) {
	int left = 0;
    int right = nums.size() - 1; // 定义target在左闭右闭的区间里，[left, right]
    while (left <= right) { // 当left==right，区间[left, right]依然有效，所以用 <=
        int middle = left + ((right - left) / 2);// 防止溢出 等同于(left + right)/2
        if (nums[middle] > target) {
            right = middle - 1; // target 在左区间，所以[left, middle - 1]
        } else if (nums[middle] < target) {
            left = middle + 1; // target 在右区间，所以[middle + 1, right]
        } else { // nums[middle] == target
            return middle; // 数组中找到目标值，直接返回下标
        }
    }
    // 未找到目标值
    return left; // return right + 1
}

// 版本二，[left, right)
// 左闭右开，因此当target小于nums[mid]时，mid = right; 并且循环判定条件为left < right
int searchInsert2(vector<int>& nums, int target) {
    int left = 0;
    int right = nums.size(); // 定义target在左闭右开的区间里，即：[left, right)
    while (left < right) { // 因为left == right的时候，在[left, right)是无效的空间，所以使用 <
        int middle = left + ((right - left) >> 1);
        if (nums[middle] > target) {
            right = middle; // target 在左区间，在[left, middle)中
        } else if (nums[middle] < target) {
            left = middle + 1; // target 在右区间，在[middle + 1, right)中
        } else { // nums[middle] == target
            return middle; // 数组中找到目标值，直接返回下标
        }
    }
        // 未找到目标值
        return left; // return right
    }



int main(int argc, char **argv)
{	
	vector<int> nums = {-1,0,3,5,9,12};
	printf("version 1, index is %d\n", search1(nums, 9));
	printf("version 2, index is %d\n", search2(nums, 9));
	return 0;
}