class Solution {
public:
    // 暴力法
    vector<int> sortedSquares1(vector<int>& nums) {
        // 更新数组
        for(int i = 0; i < nums.size(); i++) 
            nums[i] = nums[i] * nums[i];
        // 快速排序
        sort(nums.begin(), nums.end()); 
        return nums;
    }

    // 对撞指针法
    vector<int> sortedSquares2(vector<int>& nums) {
        int leftIndex = 0, rightIndex = nums.size() - 1;
        // k指向新数组的末端
        int k = rightIndex;
        // 新创建一个数组，用于存放排序后的元素
        vector<int> result(nums.size(), 0);
        // 数组其实是有序的， 只不过负数平方之后可能成为最大数了，那么数组平方的最大值就在数组的两端，
        // 不是最左边就是最右边，不可能是中间。因此从数组的两端开始比较，较大的数一定是当前数组中最大的平方数
        while(leftIndex <= rightIndex) {
            // 左指针元素值平方小于右指针元素值平方，即能确定右指针元素值平方是当前数组中最大的平方数
            if(nums[leftIndex] * nums[leftIndex] < nums[rightIndex] * nums[rightIndex]) {
                // 从大到小更新result数组的元素
                result[k--] = nums[rightIndex] * nums[rightIndex];
                rightIndex --;
            } 
            // 反之同理
            else {
                result[k--] = nums[leftIndex] * nums[leftIndex];
                leftIndex ++;
            }
        }
        // 返回结果数组
        return result;
   }
};