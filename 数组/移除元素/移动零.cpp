// 快慢指针
void moveZeroes(vector<int>& nums) {
     int slow = 0, fast = 0;
     // 慢指针表示新数组中最后一个元素的下一个下标，一开始新数组中没有元素，因此它的值为 0
     while(fast < nums.size()) {
        if(nums[fast] == 0)
            fast++;
        else {
            swap(nums[slow], nums[fast]);
            slow++;
            fast++;
        }
     }

}

// 对撞指针，可以将0移除后面，但无法保持非零元素的相对顺序
void moveZeroes2(vector<int>& nums) {
    int leftIndex = 0, rightIndex = nums.size() - 1, swap = 0;
    // 在每次循环中首先确定leftIndex和rightIndex的值，并且若前者小于后者
    // 则将对应元素值进行交换，最后更新leftIndex和rightIndex的值
    while(leftIndex <= rightIndex) {
        // 查找符合条件的元素
        while(leftIndex <= rightIndex && nums[leftIndex] != 0)
            leftIndex ++;
        while(leftIndex <= rightIndex && nums[rightIndex] == 0)
            rightIndex --;
        // 交换元素值并更新leftIndex和rightIndex的值
        if(leftIndex < rightIndex) {
            swap = nums[leftIndex];
            nums[leftIndex] = nums[rightIndex];
            nums[rightIndex] = swap;
            leftIndex ++;
            rightIndex --; 
        }
    }
    
}