// 暴力解法
int removeElement1(vector<int>& nums, int val) {
    int size = nums.size();
    for(int i = 0; i < size; i++) {
        if(nums[i] == val) { // 发现需要移除的元素，就将数组集体向前移动一位
            for(int j = i + 1; j < size; j++) {
                nums[j-1] = nums[j];
            }
            i--; // 因为下标i以后的数值都向前移动了一位，所以i也向前移动一位
            size--; // 此时数组的大小-1
        }
    }
    return size;
}

// 双指针法之快慢指针
// 两个指针从同一侧开始遍历数组，将这两个指针分别定义为快指针(fast)和慢指针(slow)，两个指针以不同的策略移动
// 在本题中快指针每次都增长，而慢指针则是条件增长
int removeElement2(vector<int>& nums, int val) {
    // 快慢指针的初始值
    int slow = 0, fast = 0;
    // 遍历数组，快指针是每次都增长，而慢指针只有在 nums[fast] != val 才会增长
    // 遍历完数组之后，慢指针的值就是新数组中最后一个元素的索引，因此需要加1返回 
    while(fast < nums.size()) {
        if(nums[fast] == val)
            fast++;
        else {
            nums[slow] = nums[fast];
            slow++;
            fast++;
        }
    }
    return slow++;
}

// 双指针法之对撞指针
// 对撞指针是指在有序数组中，将指向最左侧的索引定义为左指针(left)，最右侧的定义为右指针(right)，然后从两头向中间进行数组遍历
// 在本题中，每次循环中左指针寻找的是等于target的元素(遇到不等于target的元素则直接跳过)，右指针同理。
// 在两个指针都找到对应的元素后便进行交换，并且左指针索引加一，右指针索引减一，最后继续执行下一趟循环。
int removeElement(vector<int>& nums, int val) {
    int leftIndex = 0;
    int rightIndex = nums.size() - 1;
    while (leftIndex <= rightIndex) {
        // 找左边等于val的元素
        while (leftIndex <= rightIndex && nums[leftIndex] != val){
            ++leftIndex;
        }
        // 找右边不等于val的元素
        while (leftIndex <= rightIndex && nums[rightIndex] == val) {
            -- rightIndex;
        }
        // 将右边不等于val的元素覆盖左边等于val的元素
        if (leftIndex < rightIndex) {
            nums[leftIndex++] = nums[rightIndex--];
        }
    }
    return leftIndex;   // leftIndex一定指向了最终数组末尾的下一个元素
}