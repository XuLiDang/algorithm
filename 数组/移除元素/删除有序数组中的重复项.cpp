// 暴力解法
int removeDuplicates1(vector<int>& nums) {
    int size = nums.size();
    // 遍历数组中的元素
    for(int i = 0; i < size; i++) {
        if(i == size - 1) 
            return size;
        // nums[i]与下一个元素重复
        if(nums[i] == nums[i + 1]) {
            // 更新数组
            for(int j = i + 1; j < size; j++) {
                nums[j - 1] = nums[j];
            }
            size--; // 此时数组的大小-1
            i--; // 因为下标i以后的数值都向前移动了一位，所以i也向前移动一位
        }                       
    }
    return size;
}

// 快慢指针解法
int removeDuplicates2(vector<int>& nums) {
        // 快慢指针初始指向数组的下标 1 与 0
        int slow = 0, fast = 1;
        // 遍历数组，快指针是每次都增长，而慢指针只有在 nums[slow] != nums[fast] 时才会增长
        // 慢指针代表的是新数组当前最后一个元素的下标，而快指针则是有可能成为新数组下一个元素的下标
        while(fast < nums.size()) {
            // 两个元素的值相同，代表当前快指针指向的元素与新数组当前最后一个元素的值相同
            // 因此其不能成为新数组的下一个元素
            if(nums[slow] == nums[fast])
                fast++;
            // 两个元素的值不同，表示快指针指向的元素可以加入到新数组
            else {
                slow++;
                nums[slow] = nums[fast];
                fast++;
            }
        }
        return slow + 1;
    }