class Solution {
public:
    bool backspaceCompare(string s, string t) {
        // 处理字符串s，并获取处理过后的字符串长度
        int sLen = getLength(s);
        // 处理字符串t，并获取处理过后的字符串长度
        int tLen = getLength(t);
        // 处理过后的字符串长度不相等，则两者肯定不相等
        if(sLen != tLen)
            return false;
        // 比较处理过后的每一个字符
        for(int i = 0; i < sLen; i++) {
            if(s[i] != t[i])
                return false;
        }

        return true;
    }

    // 处理传入的字符串，并返回新字符串的长度，注意形参要加上引用
    int getLength(string &s) {
        int slow = 0, fast = 0;
        // slow 表示新字符串当前最后一个元素的下标 + 1，初始为 0 则代表新字符串中还没有元素
        // fast 则用于遍历旧字符串，要分别处理 s[fast] 等于与不等于 '#' 的情况。
        while(fast < s.size()) {
            if(s[fast] == '#') {
                slow--;
                if(slow < 0)
                    slow = 0;
                fast++;
            }
            else {
                s[slow] = s[fast];
                slow++;
                fast++;
            }
        }
        return slow;
    }
};