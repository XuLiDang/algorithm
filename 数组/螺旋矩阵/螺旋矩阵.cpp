class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        int rows = matrix.size();
        int columns = matrix[0].size();
        // 返回数组
        vector<int> res(rows * columns);
        int left = 0, top = 0, right = columns - 1, bottom = rows - 1;
        // i代表行索引，y代表列索引
        int i,j;
        // 记录已经遍历的元素个数
        int count = 1;
        // 返回数组中的索引
        int index = 0;

        while(count <= rows * columns) {
            // 最上面的一行，从左到右遍历(左闭右闭)
            for(j = left; j <= right; j++) {
                 res[index++] = matrix[top][j];
                 count ++;
            }
            // 最上面的一行已遍历完毕
            top ++;
            // 如果此时上边大于下边，说明此时已经遍历完成了，直接 break
            if(top > bottom) break;


            // 最右边的一列，从上到下遍历(左闭右闭)
            for(i = top; i <= bottom; i++) {
                res[index++] = matrix[i][right];
                count ++;
            }
            // 最右边的一列已遍历完毕
            right --;
            // 如果此时左边大于右边，说明此时已经遍历完成了，直接 break
            if(right < left) break;


            // 最下面的一行，从右到左遍历(左闭右闭)
            for(j = right; j >= left; j--) {
                res[index++] = matrix[bottom][j];
                count ++;
            }
            // 最下面的一行已遍历完毕
            bottom --;
            // 如果此时上边大于下边，说明此时已经遍历完成了，直接 break
            if(top > bottom) break;


            // 最左边的一列，从下到上遍历(左闭右闭)
            for(i = bottom; i >= top; i--) {
                res[index++] =matrix[i][left];
                count ++;
            }
            // 最左边的一列已遍历完毕
            left ++;
            // 如果此时左边大于右边，说明此时已经遍历完成了，直接 break
            if(right < left) break;
        }

        return res;
    }
};