class Solution {
public:
    vector<vector<int>> generateMatrix(int n) {
       // 使用vector定义一个二维数组
        vector<vector<int>> res(n,vector<int>(n,0));
        // 用来给矩阵中各元素赋值
        int count = 1;
        // 矩阵的左，上，右，下起始位置
        int left = 0, top = 0, right = n - 1, bottom = n - 1;
        // i代表行索引，y代表列索引
        int i,j;

        while(count <= n * n) {
            // 模拟填充上行从左到右(左闭右闭)
            for(j = left; j <= right; j++)
                res[top][j] = count ++;
            // 上面一行已被填充完毕
            top ++;
            
            // 模拟填充右列从上到下(左闭右闭)
            for(i = top; i <= bottom; i++) 
                res[i][right] = count ++;
            // 右边一列已被填充完毕
            right --;

             // 模拟填充下行从右到左(左闭右闭)
            for(j = right; j >= left; j--)
                res[bottom][j] = count ++;
            // 下面一行已被填充完毕
            bottom --;

             // 模拟填充左列从下到上(左闭右闭)
            for(i = bottom; i >= top; i--)
                res[i][left] = count++;
            // 左边一列已被填充完毕
            left ++;       
        }

        return res;
    }
};