/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
        ListNode *fast = head, *slow = head;
        // 因为快指针每次都要移动两步，因此需要确保 fast 与 fast->next 都不能为空
        while(fast != NULL && fast->next != NULL) {
            fast = fast->next->next;
            slow = slow->next;

            // 两者相遇则代表链表中一定有环
            if(fast == slow) {
                // index1是头指针，index2则是指向两者相遇时的节点的指针
                ListNode *index1 = head;
                ListNode *index2 = fast;
                // 分别从index1和index2出发，链表有环的话，两者一定会相遇
                // 并且两者相遇的节点一定就是环的入口点
                while(index1 != index2) {
                    index1 = index1->next;
                    index2 = index2->next;
                }
                // 返回环入口点
                return index2;
            }
        }
        // 链表中无环
        return NULL;
     }
};