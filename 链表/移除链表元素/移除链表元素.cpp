/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        // 创建一个虚拟头结点，并用dummyHead指针指向它
        ListNode *dummyHead = new ListNode(0);
        // cur指针同样指向虚拟头结点
        ListNode *cur = dummyHead;
        // 使虚拟头结点的next指针指向链表真正的头结点
        dummyHead->next = head;

        // 遍历链表，找出要删除的结点
        while(cur->next != NULL) {
            if(cur->next->val == val) {
                ListNode *tmp = cur->next;
                cur->next = tmp->next;
                delete(tmp);
            }
            else {
                cur = cur->next;
            }
        }

        // 删除完毕后，链表原先的头结点有可能会被删除
        // 因此需要更新head指针，使其指向新的头结点 
        head = dummyHead->next;
        // 删除所申请的虚拟头结点
        delete(dummyHead);
        // 返回头结点指针
        return head;
    }
};