class MyLinkedList {
public:
    // 定义链表节点结构体
    struct LinkedNode {
        int val;
        LinkedNode* next;
        LinkedNode(int val):val(val), next(nullptr){}
    };

     // 初始化链表
    MyLinkedList() {
        // 创建一个虚拟头结点
        __dummyHead = new LinkedNode(0);
        // 记录链表的长度
        __size = 0;
    }
    
    // 获取到第index个节点数值，如果index是非法数值直接返回-1， 注意index是从0开始的，第0个节点就是头结点
    int get(int index) {
        // 索引无效返回-1  
        if(index > __size - 1 || index < 0) 
            return -1;

        // cur指针指向头结点
        LinkedNode *cur = __dummyHead->next;
        // 遍历链表，查找索引为index的结点
        while(index) {
            cur = cur->next;
            index--;
        }

        return cur->val;
    }
    
    void addAtHead(int val) {
        // 创建新结点
        LinkedNode *newNode = new LinkedNode(0);
        // 为新结点赋值
        newNode->val = val;
        // 在链表的第一个元素之前添加一个值为 val 的节点
        newNode->next =  __dummyHead->next;
        __dummyHead->next = newNode;
        // 增加链表的长度
        __size ++;
    }
    
    void addAtTail(int val) {
        // cur指针指向虚拟头结点
        LinkedNode *cur = __dummyHead;
        // 创建新结点
        LinkedNode *newNode = new LinkedNode(0);
        // 为新结点赋值
        newNode->val = val;

        // 遍历链表，直到cur指向链表的最后一个结点
        while(cur->next != nullptr)
            cur = cur->next;

        // 此时cur指向的是最后一个结点
        // 将值为 val 的节点追加到链表的最后一个元素
        newNode->next = cur->next;
        cur->next = newNode;
        // 增加链表的长度
        __size ++;
     }
    
    // 在链表中的第 index 个节点之前添加值为 val  的节点。
    void addAtIndex(int index, int val) {
        // 如果 index 大于链表长度，则不会插入节点
        if(index > __size)
            return;
        // 如果 index 小于0，则在头部插入节点
        if(index < 0) index = 0;
      
        // cur指针指向虚拟头结点
        LinkedNode *cur = __dummyHead;
        // 创建新结点
        LinkedNode *newNode = new LinkedNode(0);
        // 为新结点赋值
        newNode->val = val;

        while(index) {
            index--;
            cur = cur->next;
        }
        
        newNode->next = cur->next;
        cur->next = newNode;
        __size++;
    }
    
    // 删除索引为index的结点
    void deleteAtIndex(int index) {
        // 无效索引直接返回
        if(index < 0 || index > __size - 1)
            return;
        // cur指针指向头结点
        LinkedNode *cur = __dummyHead;
        // 临时指针
        LinkedNode *tmp;

        while(index) {
            index--;
            cur = cur->next;
        }

        tmp = cur->next;
        cur->next = tmp->next;
        delete tmp;
        __size--;
    }

private:
    int __size;
    LinkedNode* __dummyHead;
};


// // 在链表中的第 index 个节点之前添加值为 val  的节点。
//     void addAtIndex(int index, int val) {
//         // 如果 index 大于链表长度，则不会插入节点
//         if(index > __size)
//             return;
//         // 如果 index 小于等于0，则在头部插入节点
//         if(index <= 0) {
//             addAtHead(val);
//             return;
//         }
//         if(index == __size) {
//             addAtTail(val);
//             return;
//         }
      
//         // cur指针指向虚拟头结点
//         LinkedNode *cur = __dummyHead;
//         // 创建新结点
//         LinkedNode *newNode = new LinkedNode(0);
//         // 为新结点赋值
//         newNode->val = val;

//         while(index) {
//             index--;
//             cur = cur->next;
//         }
        
//         newNode->next = cur->next;
//         cur->next = newNode;
//         __size++;
//     }