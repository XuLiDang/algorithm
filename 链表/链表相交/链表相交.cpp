/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    // 这里需要注意一个点: 相交节点之后的所有节点都是一样的，那么根据这个前提我们可以得知
    // 对于长度较长的链表来说，其与长度较短的链表尾部对齐后，超出的那部分不可能是相交节点
    // 因此，需要得出两个链表之间的长度差gap，并且指向长度较长链表的头节点指针要先移动gap步
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        ListNode *curA = headA, *curB = headB;
        int lenA = 0, lenB = 0, gap = 0;

        // 求链表A的长度
        while(curA != NULL) {
            lenA++;
            curA = curA->next;
        }

        // 求链表B的长度
        while(curB != NULL) {
            lenB++;
            curB = curB->next;
        }

        // 使得curA和curB重新指向链表的头结点
        curA = headA;
        curB = headB;

        // 让curA为较长链表的头节点指针，lenA为其长度
        if(lenA < lenB) {
            // 交换curA与curB的值
            swap(curA, curB);
            // 交换lenA与lenB的值
            swap(lenB, lenA);;       
        }

        // 获得两条链表之间的长度差
        gap = lenA - lenB;
        // curA指针首先往前移动gap步
        while(gap) {
            gap--;
            curA = curA->next;
        }

        // 查找相交的节点
        while(curA != NULL) {
            if(curA == curB){
                return curA;
            }      
            curA = curA->next;
            curB = curB->next;          
        }

        return NULL;
    }
};