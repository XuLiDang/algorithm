/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        ListNode *dummyHead = new ListNode(0);
        ListNode *tmp;
        // 快慢指针初始时指向虚拟头结点
        ListNode *fast = dummyHead, *slow = dummyHead;
        // 虚拟头结点的下一个结点便是头结点
        dummyHead->next = head;

        // 采用快慢指针法，快指针先移动n步，随后快慢指针同时移动
        // 那么当快指针遍历完链表并且指向NULL时，慢指针便是只需再
        // 移动n步就能指向NULL，即它指向的是链表中的倒数第n个元素
        while(n + 1) {
            fast = fast->next;
            n--;
        }

        while(fast != NULL) {
            slow = slow->next;
            fast = fast->next;
        }

        tmp = slow->next;
        slow->next = tmp->next;
        delete tmp;
        
        // dummyHead->next是更新之后的指向头结点的指针
        // 原先head指针所指向的头结点可能会被删除
        return dummyHead->next;
    }
};