/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* reverseList(struct ListNode* head){
    struct ListNode *pre = NULL; // cur的前一个结点
    struct ListNode *cur = head; // cur指向的是当前结点
    struct ListNode *tmp; // 保存cur的下一个结点，防止断链

    // 翻转链表要做的只是掉转各结点next指针的方向。因此整体逻辑为：首先保存当前结点的下一个结点防止断链，
    // 随后当前节点的next指针修改为当前节点的上一个结点来掉转next指针的方向，最后当前结点和前一个结点分别
    // 向前移动，循环结束的条件是cur结点为空结点，循环结束后，前一个结点便是新链表的头结点
    while(cur != NULL) {
        tmp = cur->next;
        cur->next = pre;
        pre = cur;
        cur = tmp;
    }

    return pre;
}



// 递归法实现反转链表
struct ListNode* reverse(struct ListNode* pre, struct ListNode* cur) {
    // 递归结束的条件
    if(cur == NULL) return pre;
    // 保存当前节点的下一个节点
    ListNode *tmp = cur->next;
    // 掉转当前结点的next指针方向
    cur->next = pre;
    // 将剩下的工作交给下一个递归函数
    return reverse(cur,tmp);
}

struct ListNode* reverseList(struct ListNode* head){
    ListNode *pre = NULL;
    ListNode *cur = head;
    return reverse(pre,head);
}