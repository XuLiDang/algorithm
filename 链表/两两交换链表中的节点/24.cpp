/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        ListNode *dummyHead = new ListNode(0);
        ListNode *cur = dummyHead;
        ListNode *tmp1,*tmp2;
        dummyHead->next = head;

        // 初始时cur指向虚拟头结点，并且每次都将cur之后的两个节点进行交换
        // 因此cur之后的两个结点必须得存在才能交换
        while(cur->next != NULL && cur->next->next != NULL) {
            // 保存cur之后的两个结点
            tmp1 = cur->next;
            tmp2 = cur->next->next;
            // 交换过程
            tmp1->next = tmp2->next;
            tmp2->next = tmp1;
            cur->next = tmp2;
            // cur指针移动两位，准备下一轮的交换
            cur = cur->next->next;
        }

        // 更新头指针所指向的结点
        head = dummyHead->next;
        return head;
    }
};